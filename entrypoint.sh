#!/bin/sh
# entrypoint.sh

echo "Create static dir"

mkdir -p static
mkdir -p media
mkdir -p staticfiles

echo "Collect static files"

python manage.py collectstatic --noinput

python manage.py compilemessages

echo "Migrate app"

python manage.py migrate

if [ ! -z $SUPER_USER ]; then
    echo "Create staff user ${SUPER_USER} ... "
    python manage.py create_staff_user -u $SUPER_USER -p $SUPER_USER_PASS -s
fi

gunicorn emacenter.wsgi:application \
  --bind :8000 \
  --workers 3 \
  --access-logfile access_gunicorn.log \
  --error-logfile error_gunicorn.log
