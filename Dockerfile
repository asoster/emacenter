# pull official base image
FROM python:3.8-slim
LABEL mainteiner=asoster

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# set work directory
WORKDIR /app

RUN apt-get update && apt-get install -y gettext && apt-get clean -y

COPY requirements.txt /app/

RUN pip install --upgrade pip \
    && pip install --upgrade --no-warn-script-location -r requirements.txt

# copy project
COPY . /app/

# CMD will run when this dockerfile is running
CMD ["sh", "entrypoint.sh"]
