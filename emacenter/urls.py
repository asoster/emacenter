"""
emacenter URL Configuration
"""

import sys
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib.admin import site

urlpatterns = [
    url(r'^', include('climapp.urls')),
    url(r'^admin/', site.urls),
    url(r'^accounts/',
        include('django_registration.backends.activation.urls')),
    url(r'^accounts/', include('django.contrib.auth.urls')),
]

if settings.DEBUG or sys.argv[1] == 'test':
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
