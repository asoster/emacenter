| Name | Version | License | Author |
|------|---------|---------|--------|
| Django | 2.2.4 | BSD | Django Software Foundation |
| Jinja2 | 2.10.1 | BSD | Armin Ronacher |
| MarkupSafe | 1.1.1 | BSD-3-Clause | Armin Ronacher |
| certifi | 2019.6.16 | MPL-2.0 | Kenneth Reitz |
| chardet | 3.0.4 | LGPL | Daniel Blanchard |
| confusable-homoglyphs | 3.2.0 | MIT | Victor Felder |
| coreapi | 2.3.3 | BSD | Tom Christie |
| coreschema | 0.0.4 | BSD | Tom Christie |
| coverage | 4.5.4 | Apache 2.0 | Ned Batchelder and 100 others |
| django-bootstrap-static | 4.0.0 | MIT | Peter Bittner |
| django-cors-headers | 3.1.0 | MIT License | Otto Yiu |
| django-registration | 3.0.1 | UNKNOWN | James Bennett |
| djangorestframework | 3.10.2 | BSD | Tom Christie |
| gunicorn | 19.9.0 | MIT | Benoit Chesneau |
| idna | 2.8 | BSD-like | Kim Davies |
| itypes | 1.1.0 | BSD | Tom Christie |
| olefile | 0.46 | BSD | Philippe Lagadec |
| pipdeptree | 0.13.2 | MIT License | Vineet Naik |
| psycopg2 | 2.8.3 | LGPL with exceptions or ZPL | Federico Di Gregorio |
| pytz | 2019.2 | MIT | Stuart Bishop |
| requests | 2.22.0 | Apache 2.0 | Kenneth Reitz |
| setproctitle | 1.1.10 | BSD | Daniele Varrazzo |
| sqlparse | 0.3.0 | BSD | Andi Albrecht |
| uritemplate | 3.0.0 | BSD 3-Clause License or Apache License, Version 2.0 | Ian Cordasco |
| urllib3 | 1.25.3 | MIT | Andrey Petrov |
