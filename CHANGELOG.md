
# Lista de cambios en EMA Libre Center (capibara)

## v1.1.0 (Marzo 2021)

### Mejoras

- Se agrega el modelo Calidad
- Se agrega el campo tanque al modelo Registro
- Se genera Dockerfile para mejorar el despliegue de versiones
- Agregado environment.conf para configuración del container
- Queda deprecado el despliegue por medio de guiones de shell


## v1.0.4 (Noviembre 2019)

### Mejoras

- se agrega gpl 3 a cada uno de los archivos del proyecto
- se agrega archivos AUTHORS y cambia LICENSE por COPYING
- actualización de apps (django 2.2.4, cors 3.1.0 y rest 3.10.2)
- mejora de api rest: lista de estaciones y uso de timezone local
- se pasa host desde apache a la app
- se corrigen nombres de los vientos
- populate_db genera registros de un día para atrás para ranking de estaciones
- ranking de estaciones vía rest api
- envio de correo para estaciones online/offline vía batch

## v1.0.3 (Julio 2019)

### Mejoras

- se agrega registro de usuario separado de la estación (django-registration)
- se agrega aplicación sites
- nueva documentación del api rest
- actualización de apps (django 2.2.3, cors 3.0.2 y rest 3.9.4)
- mejora de la documentación en el readme
- se mejoran los scripts bash para deploy y log de instalación
- se mueven los templates dentro de la aplicación
- mejora en envío de correo y mejoras de las clases del api rest
- se usa la configuración para mostrar el nombre de la app
- se permite la modificación de datos del usuario
- migrado a bootstrap 4
- mejoras en los templates html y arreglos en tests
- directorio deploy renombrado para facilitar el uso
- se agrega favicon de capibara
- páginas de error usando sólo bootstrap
- implementado jquery y bootstrap vía app en django
- se agregan licencias GNU GPL en archivos separados
- se remueven archivos static (css y js) innecesarios
- mejoras del bash de actualización

## v1.0.2 (Mayo 2019)

### Mejoras

- mejoras en la longitud de las líneas en templates html y en html en general
- se agrega documentación en readme y el changelog
- mejoras en la vista nested del api rest de estaciones y registros y en la 
serialización de datos
- mejoras en validaciones y en la longitud de lineas en models
- mejoras en los test unitarios y se autamatiza la covertura de test unitarios
- mejoras de la configuración del sitio
- se agrega documentación de api rest y paquetes necesarios
- agregados campos a user para api rest
- no se mantiene control de cambios de .coverage
- mejora del llenado automático (populated_db) de datos de la base de dasarrollo, 
evitando populate solo cuando las estaciones ya existen
- mejora en el bash de actualización
- actualizado django 2.1.7
- se agregan estadísticas de la estación para las últimas 24 hs
- mejora del armado y validación del acumulado de lluvias
- removido template de admin y modificado el título dentro del admin
- se usa el nombre para mostrar datos de la estacion
- mejora en la configuración de la descripción
- se mantiene registro de cambios de migrations
- mejorado el manejo de archivos static

## v1.0.1 (Septiembre 2018)

### Mejoras

- agregado el api rest con viewset y serializers
- mejora en el template del formulario de estaciones
- mejora en los decoradores de las vistas
- mejora configuración de login
- se agregra cors headers
- actualización django2 y actualización de los requeriments
- se agrega robots.txt
- se agrega edición de estaciones
- se agrega html static para errores en servidor web
- se corrige post de datos de registros y validación de acumulado
- se agregan del acumulado de lluvia y tests unitarios
- se agrega timezone al formateo de fecha isoformat. Se agregan testeos de fechas
- se calcula el acumulado de lluvia por hora y día
- mejoras en las vistas, modelos, templates y las búsquedas del admin
- se mejora el envío de correo y la configuración
- se agrega app rest
- se agregan populate de registros
