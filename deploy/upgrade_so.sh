#!/bin/bash
#
# upgrade_so.sh
#
# LICENSE:  This file is part of EMA Libre.
# EMA Libre is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EMA Libre is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EMA Libre.  If not, see http://www.gnu.org/licenses/.
#
# @copyright  Copyright (c) 2019 GUGLER (http://www.gugler.com.ar)
# @license    https://www.gnu.org/licenses/gpl-3.0.html GPL License
# @link       https://www.gugler.com.ar
# @since      File available since Release v1.0.0


if [ -e deploy/libs.sh ]; then . deploy/libs.sh; fi

if [[ ! ${ENTORNO+x} ]]; then 
    export ENTORNO=DEV
fi

export INSTALL="apt install -y "

export UPGRADE_SO="apt update && apt upgrade -y && apt dist-upgrade -y "

echo_end "Configuración del ${GREEN}Sistema Operativo ${NC} ... "

${UPGRADE_SO}

SISTEMA_DEV=" pwgen sed rsync gcc python3 python3-virtualenv python3-pip \
    python3-pil python3-dev libpq-dev gettext libjpeg-dev libtiff-dev \
    libfreetype6-dev git "

SISTEMA_PROD=" apache2 postgresql postgresql-contrib sudo "

for f in $SISTEMA_DEV; do
    echo_end "Instalando ${BLUE}${f}${NC} ... "
    ${INSTALL} $f 2> /dev/null >> $2
    echo_listo
done

if [[ $ENTORNO == PROD ]]; then
    for f in $SISTEMA_PROD; do
        echo_nend "Instalando ${BLUE}${f}${NC} ... "
        ${INSTALL} -y $f 2> /dev/null >> $2
        echo_listo
    done
fi

echo_end "[ ${GREEN}listo${NC} ]"
