#!/bin/bash
#
# deploy_nginx.sh
#
# LICENSE:  This file is part of EMA Libre.
# EMA Libre is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EMA Libre is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EMA Libre.  If not, see http://www.gnu.org/licenses/.
#
# @copyright  Copyright (c) 2019 GUGLER (http://www.gugler.com.ar)
# @license    https://www.gnu.org/licenses/gpl-3.0.html GPL License
# @link       https://www.gugler.com.ar
# @since      File available since Release v1.0.0


set -e

apt install -y nginx 

cat > /etc/nginx/sites-available/nginx.conf << BLOQUE
# nginx.conf

upstream test_server {
    server unix:${WEBDIR}/run/gunicorn.sock fail_timeout=10s;
}

#~ server {
    #~ listen 80;
    #~ server_name www.${URLWEB};
    #~ return 301 $scheme://www.${URLWEB}$request_uri;
#~ }

server {
    listen 80;
    server_name ${URLWEB};

    client_max_body_size 4G;

    access_log ${WEBDIR}/log/nginx-access.log;
    error_log ${WEBDIR}/log/nginx-error.log warn;

    location = /favicon.ico {
        access_log off;
        log_not_found off;
    }

    location /static/ {
        autoindex on;
        alias ${DJANGODIR}/static/;
    }

    location /media/ {
        autoindex on;
        alias ${DJANGODIR}/media/;
    }

    location / {
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header Host \$http_host;
        proxy_redirect off;

        if (!-f \$request_filename) {
            proxy_pass http://test_server;
            break;
        }
    }

    #For favicon
    location /favicon.ico {
        alias ${DJANGODIR}/static/img/favicon.ico;
    }
    #For robots.txt
    location /robots.txt {
        alias ${DJANGODIR}/static/robots.txt ;
    }
    # Error pages
    error_page 500 502 503 504 /500.html;
    location = /500.html {
        root ${DJANGODIR}/static/;
    }
}

BLOQUE

rm -f /etc/nginx/sites-enabled/${APP}

ln -s /etc/nginx/sites-available/nginx.conf /etc/nginx/sites-enabled/${APP}

systemctl stop nginx

systemctl start nginx

systemctl status nginx
