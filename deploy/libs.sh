#!/bin/bash
#
# libs.sh
#
# LICENSE:  This file is part of EMA Libre.
# EMA Libre is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EMA Libre is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EMA Libre.  If not, see http://www.gnu.org/licenses/.
#
# @copyright  Copyright (c) 2019 GUGLER (http://www.gugler.com.ar)
# @license    https://www.gnu.org/licenses/gpl-3.0.html GPL License
# @link       https://www.gugler.com.ar
# @since      File available since Release v1.0.0


export BLACK='\033[0;30m'
export GRAY='\033[1;30m'
export RED='\033[0;31m'
export LRED='\033[1;31m'
export GREEN='\033[0;32m'
export LGREEN='\033[1;32m'
export ORANGE='\033[0;33m'
export YELLOW='\033[1;33m'
export BLUE='\033[0;34m'
export LBLUE='\033[1;34m'
export PURPLE='\033[0;35m'
export LPURPLE='\033[1;35m'
export CYAN='\033[0;36m'
export LCYAN='\033[1;36m'
export LGRAY='\033[0;37m'
export WHITE='\033[1;37m'
export NC='\033[0m' # No Color


echo_log(){
    echo -e "$(date '+%F %T%Z') ${*}" >> ${LOG_FILE}
}


echo_nend(){
    echo_log "${*}"
    echo -en "${*}"
}


echo_end(){
    echo_log "${*}"
    echo -e "${*}"
}


echo_listo(){
    echo_end "[ ${GREEN}listo${NC} ]"
}


activar_venv(){
    dir_actual="$( cd "$(dirname .)" ; pwd -P )"
    if [ -e venv/bin/activate ]; then
        echo_nend "Activando entorno virtual en ${RED}${dir_actual}${NC} ... "
        . venv/bin/activate
        echo_listo
    fi
}


prepara_venv(){
    echo_end "Preparando entorno virtual en ${BLUE}${1}${NC} ... "
    if [ ! -e venv/bin/activate ]; then
       python3 -m virtualenv --python=python3 --always-copy venv
    fi
    activar_venv
    #~ pip freeze >> /dev/null
    pip install --upgrade -r requeriments.txt >> /dev/null
    echo_listo
}


actualiza_git(){
    pushd ${1} >> /dev/null
    echo_nend "Actualizando repositorio ${RED}${2}${NC} de ${BLUE}${APP}${NC} ... "
    git pull ${2} master
    echo_listo
    popd >> /dev/null
}


actualiza_app(){
    echo_nend "Actualizando aplicación ${BLUE}${APP}${NC} ... "
    activar_venv
    ./manage.py check
    ./manage.py makemigrations
    ./manage.py migrate
    ./manage.py collectstatic --noinput
    echo_listo
}


add_user(){
    activar_venv
    echo_nend "Agregando usuario ${RED}${1}${NC} a ${BLUE}${APP}${NC} ... "
    ./manage.py shell << BLOQUE
from django.contrib.auth.models import User
User.objects.create_superuser('${1}', '${2}', '${3}')
BLOQUE
    echo_listo
}


populate_db(){
    activar_venv
    echo_nend "Agregando datos de prueba a ${BLUE}${APP}${NC} ... "
    ./manage.py populate_db
    echo_listo
}


run_db_query(){
    echo_nend "Ejecutando DB Query ${BLUE}${1}${NC} ... "
    su - postgres -c " psql -c \"${1}\" "
    echo_listo
}


reinicio_servicios(){
    echo_end "Reinicializando servicio ${BLUE}${1}${NC} ... "
    systemctl restart ${1}
    systemctl status ${1}
    echo_listo
}


desactiva_servicio(){
    echo_end "Desactivando servicio ${BLUE}${1}${NC} ... "
    systemctl stop ${1}
    systemctl disable ${1}
    systemctl daemon-reload
    systemctl reset-failed
    echo_listo
}


activa_servicio(){
    echo_end "Activando servicio ${BLUE}${1}${NC} ... "
    systemctl start ${1}
    systemctl enable ${1}
    systemctl status ${1}
    echo_listo
}


actualiza_dev(){
    pushd ${APPREPO} >> /dev/null
    actualiza_git ${APPREPO} origin
    prepara_venv
    actualiza_app
    popd
}


copia_archivos(){
    pushd ${DJANGODIR} >> /dev/null
    echo_end "Actualizando archivos en ${DJANGODIR} ... "
    rsync -av --exclude='venv' \
        --exclude='.git' --exclude='.gitignore' \
        --exclude='db.sqlite3' --exclude='deploy' \
        --exclude='*/__pycache__' --exclude='*.pyc' \
        --exclude='emacenter/develop.py' \
        --exclude='emacenter/production.py' \
        ${APPREPO}/ ${DJANGODIR}/ >> /dev/null
    echo_listo
    popd
}


actualiza_prod(){
    echo_end "Configurando entorno productivo ... "
    if [ -e ${DJANGODIR} ]; then
        pushd ${DJANGODIR} >> /dev/null
        prepara_venv ${DJANGODIR} venv ${DJANGODIR}
        actualiza_app
        popd
    fi
    echo_listo
}


secret_key_config(){
    echo_nend "Escribiendo configuración en ${BLUE}${1}${NC}"
    pushd ${DJANGODIR}/${APP} >> /dev/null

    if [[ $UPGRADE_APP == NO ]]; then
        sed -i "s/<SECURE_SECRET_KEY>/${SECRET_KEY}/g" ${1} 
    else
python3 << BLOQUE
try:
    from production import SECRET_KEY
except:
    from develop import SECRET_KEY
s = ''
with open('settings.py') as ori:
    s = ori.read().replace('<SECURE_SECRET_KEY>', SECRET_KEY)
if s:
    with open('settings.py', 'w') as f:
        f.write(s)

BLOQUE
    fi
    popd
    echo_listo
}


secret_config_db(){
echo_nend "Escribiendo configuración en ${BLUE}${1}${NC}"
cat >> ${1} << BLOQUE
#${1}
#    creado $(date --iso-8601=ns)

#Configuración de Bases de datos
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '${DB_NAME}',
        'USER': '${DB_USER}',
        'PASSWORD': '${DB_PASS}',
        'HOST': '${DB_HOST}',
        'PORT': '',
    }
}

BLOQUE
echo_listo
}

secret_config(){
    echo_nend "Escribiendo configuración en ${BLUE}${1}${NC}"
cat >> ${1} << BLOQUE
#${1}
#    creado $(date --iso-8601=ns)

#la clave del usuario ${USER_ADMIN} es ${CLAVE_ADMIN}

SECRET_KEY = '${SECRET_KEY}'

# MIS_APPS = ('climapp')

EMAIL = {
    'HOST': 'smtp.domain.server',
    'PORT': 465,
    'HOST_USER': '',
    'HOST_PASSWORD': '',
    'USE_TLS': False,
    'USE_SSL': True
}

MANAGERS = (
    ('$(whoami)', '$(whoami)@localhost'),
)

CLAVE_MAIL_OFFLINE = '${CLAVE_MAIL_OFFLINE}'

BLOQUE
    echo_listo
}


gunicorn_script(){
    echo_nend "Creando script ${BLUE}${WEBDIR}/${APP}_run.sh${NC} "
cat > ${WEBDIR}/${APP}_run.sh << BLOQUE
#!/bin/bash

#${WEBDIR}/${APP}_run.sh
#    creado $(date --iso-8601=ns)

echo -e "Iniciando ${APP}"

cd ${DJANGODIR}
source ${WEBDIR}/${APP}/venv/bin/activate

export DJANGO_SETTINGS_MODULE=${APP}.settings
export PYTHONPATH=${DJANGODIR}:\$PYTHONPATH

test -d $(dirname ${WEBDIR}/run/gunicorn.sock) || mkdir -p $(dirname ${WEBDIR}/run/gunicorn.sock)

# Iniciando Gunicorn

exec ${WEBDIR}/${APP}/venv/bin/gunicorn ${APP}.wsgi:application \
  --name ${APP} \
  --workers ${APP_WORKERS} \
  --user ${USER_WEB} \
  --group ${GROUP_WEB} \
  --bind=127.0.0.1:8000 \
  --access-logfile ${WEBDIR}/log/access_gunicorn.log \
  --error-logfile ${WEBDIR}/log/error_gunicorn.log

#    --bind=unix:${WEBDIR}/run/gunicorn.sock

BLOQUE
    chmod +rx ${WEBDIR}/${APP}_run.sh
    echo_listo
}


gunicorn_service(){
    echo_nend "Creando servicio ${BLUE}${APP}.service ${NC} "
cat > /etc/systemd/system/${APP}.service << BLOQUE
[Unit]
Description=${APP}
After=network.target

[Service]
Type=simple
User=${USER_WEB}
Group=${GROUP_WEB}
WorkingDirectory=${WEBDIR}
ExecStart=${WEBDIR}/${APP}_run.sh

[Install]
WantedBy=multi-user.target

BLOQUE
    echo_listo
}


ranking_service(){
    echo_nend "Creando servicio ${BLUE}${APP}_ranking.service ${NC} "
cat > ${WEBDIR}/${APP}_ranking.sh << BLOQUE
#!/bin/bash
wget '${URLWEB}/ranking/?clave_mail=${CLAVE_MAIL_OFFLINE}&html=si' \
    -O /tmp/${APP}_ranking.html
BLOQUE
    chmod ug+rx ${WEBDIR}/${APP}_ranking.sh
cat > /etc/systemd/system/${APP}_ranking.service << BLOQUE
[Unit]
Description=${APP} Ranking Service

[Service]
User=${USER_WEB}
Group=${GROUP_WEB}
WorkingDirectory=${WEBDIR}
ExecStart=${WEBDIR}/${APP}_ranking.sh

[Install]
WantedBy=multi-user.target
BLOQUE
    echo_listo

    echo_end "Inicializando servicio ${BLUE}${APP}_ranking${NC} ... "
    systemctl enable ${APP}_ranking.service
    systemctl start ${APP}_ranking.service
    echo_listo

    echo_nend "Creando timer ${BLUE}${APP}_ranking.timer ${NC} "

cat > /etc/systemd/system/${APP}_ranking.timer << BLOQUE
[Unit]
Description=${APP} Ranking Timer

[Timer]
OnCalendar=*-*-* 11:00:00

[Install]
WantedBy=multi-user.target
BLOQUE
    echo_listo

    echo_end "Inicializando timer ${BLUE}${APP}_ranking${NC} ... "
    systemctl enable ${APP}_ranking.timer
    systemctl start ${APP}_ranking.timer
    echo_listo
}


apache_conf(){
    echo_nend "Creando virtualhost en Apache ${BLUE}${APP}${NC} "
cat > /etc/apache2/sites-available/${APACHE_FILE} << BLOQUE
<VirtualHost *:80>
ServerName ${URLWEB}

Alias /robots.txt ${WEBDIR}/${APP}/static/robots.txt
Alias /favicon.ico ${WEBDIR}/${APP}/static/img/favicon.png
Alias /media/ ${WEBDIR}/${APP}/media/
Alias /static/ ${WEBDIR}/${APP}/static/

<Directory ${WEBDIR}/${APP}/static>
Require all granted
</Directory>

<Directory ${WEBDIR}/${APP}/media>
Require all granted
</Directory>

ProxyPreserveHost On
ProxyPass /static/ !
ProxyPass /media/ !
#~ ProxyPass / unix:${WEBDIR}/run/gunicorn.sock|http://127.0.0.1:8000/
ProxyPass / http://127.0.0.1:8000/
</VirtualHost>

BLOQUE
    echo_listo
}

enable_apache(){
    echo_nend "Activando Apache ${BLUE}${APP}${NC} "
    pushd /etc/apache2/mods-enabled
    if [[ ! -e proxy.conf ]]; then
        ln -s ../mods-available/proxy.conf .
    fi
    if [[ ! -e proxy.load ]]; then
        ln -s ../mods-available/proxy.load .
    fi
    if [[ ! -e proxy_http.load ]]; then
        ln -s ../mods-available/proxy_http.load .
    fi
    popd
    pushd /etc/apache2/sites-enabled
    if [[ ! -e ${APACHE_FILE} ]]; then
        rm -f ${APACHE_FILE}
    fi
    ln -s ../sites-available/${APACHE_FILE} .
    popd
    reinicio_servicios apache2
    echo_listo
}


uninstall_app(){
    echo_nend "Desinstalando todo de ${BLUE}${APP}${NC} "
    rm -f /etc/apache2/sites-enabled/${APACHE_FILE}
    rm -f /etc/apache2/sites-available/${APACHE_FILE}
    reinicio_servicios apache2
    desactiva_servicio ${APP}
    echo_listo
}

