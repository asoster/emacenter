#!/bin/bash
#
# deploy_dev.sh
#
# LICENSE:  This file is part of EMA Libre.
# EMA Libre is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EMA Libre is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EMA Libre.  If not, see http://www.gnu.org/licenses/.
#
# @copyright  Copyright (c) 2019 GUGLER (http://www.gugler.com.ar)
# @license    https://www.gnu.org/licenses/gpl-3.0.html GPL License
# @link       https://www.gugler.com.ar
# @since      File available since Release v1.0.0


#~ set -e

if [ -e deploy/libs.sh ]; then . deploy/libs.sh; fi

if [ -e deploy/config_app.sh ]; then . deploy/config_app.sh; fi

if [[ ${RESTART+x} ]]; then 
    deactivate
    rm -r ${APPREPO}/venv
    rm ${LOG_FILE}
    rm ${APPREPO}/${APP}/develop.py
    rm ${APPREPO}/db.sqlite3
    export UPGRADE_APP=NO
    export CREATE_USERS=SI
fi

cd ${APPREPO}

export CLAVE_ADMIN=emaAdmin1

echo_end "Configurando entorno ${LGREEN}${ENTORNO}${NC} de ${RED}${APP}${NC} ... "

#~ if [ -e deploy/upgrade_so.sh ]; then . deploy/upgrade_so.sh; fi

actualiza_dev

if [[ $UPGRADE_APP == NO ]]; then
    secret_config ${SECRET_FILE}
    # En entorno de desarrollo no genera una clave secreta
    #~ secret_key_config ${SETTINGS_FILE}
fi

if [[ $CREATE_USERS == SI ]]; then
    add_user ${USER_ADMIN} ${EMAIL_ADMIN} ${CLAVE_ADMIN}
    add_user ${USER_DEV} ${EMAIL_DEV} ${CLAVE_DEV}
    populate_db
fi

activar_venv
