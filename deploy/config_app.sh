#!/bin/bash
#
# config_app.sh
#
# LICENSE:  This file is part of EMA Libre.
# EMA Libre is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EMA Libre is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EMA Libre.  If not, see http://www.gnu.org/licenses/.
#
# @copyright  Copyright (c) 2019 GUGLER (http://www.gugler.com.ar)
# @license    https://www.gnu.org/licenses/gpl-3.0.html GPL License
# @link       https://www.gugler.com.ar
# @since      File available since Release v1.0.0


echo_nend "Configurando datos de la aplicación ... "

export APP=emacenter
export USER_WEB=www-data
export GROUP_WEB=www-data

if [[ ! ${URLWEB+x} ]]; then
    export URLWEB=emacenter.gugler.com.ar
fi

export APACHE_FILE=${URLWEB}.conf 

export APP_WORKERS=3

if [[ ! ${ENTORNO+x} ]]; then
    export ENTORNO=DEV
fi

export SECRET_KEY=$(pwgen -sv 50)
export CLAVE_MAIL_OFFLINE=$(pwgen -sv 10)

export USER_ADMIN=admin
export EMAIL_ADMIN=admin@localhost
export CLAVE_ADMIN=$(pwgen -sv 12)

export USER_DEV=$(whoami)
export EMAIL_DEV=$(whoami)@localhost
export CLAVE_DEV=$(whoami)1

export DB_HOST=localhost
export DB_NAME=${APP}
export DB_USER=ema_db_user
export DB_PASS=$(pwgen -sv 16)

export WEBDIR=/var/www/emacenter.gugler.com.ar

if [[ $ENTORNO == DEV ]]; then
    export WEBDIR=/home/${USER_DEV}/${APP}_prod
fi

export APPREPO="$( cd "$(dirname .)" ; pwd -P )"

export DJANGODIR=${WEBDIR}/${APP}

export DEPLOY_DIR=${APPREPO}/deploy

#~ if [ ! -e ${DJANGODIR} ]; then export ENTORNO=DEV; fi

export CREATE_USERS=SI

if [ -e ${APPREPO}/db.sqlite3 ]; then
    export CREATE_USERS=NO
fi

export SECRET_FILE=${APPREPO}/${APP}/develop.py
export LOG_FILE=${DEPLOY_DIR}/${APP}_install.log
export SETTINGS_FILE=${APPREPO}/${APP}/settings.py

if [[ $ENTORNO == PROD ]]; then
    export SECRET_FILE=${DJANGODIR}/${APP}/production.py
    export LOG_FILE=${WEBDIR}/log/${APP}_install.log
    export SETTINGS_FILE=${DJANGODIR}/${APP}/settings.py
fi

export UPGRADE_APP=NO

if [ -e ${SECRET_FILE} ]; then
    export UPGRADE_APP=SI
fi

echo_listo
