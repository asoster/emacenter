#!/bin/bash
#
# deploy_prod.sh
#
# LICENSE:  This file is part of EMA Libre.
# EMA Libre is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EMA Libre is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EMA Libre.  If not, see http://www.gnu.org/licenses/.
#
# @copyright  Copyright (c) 2019 GUGLER (http://www.gugler.com.ar)
# @license    https://www.gnu.org/licenses/gpl-3.0.html GPL License
# @link       https://www.gugler.com.ar
# @since      File available since Release v1.0.0


#~ set -e

export ENTORNO=PROD

if [ -e deploy/config_app.sh ]; then . deploy/config_app.sh; fi

if [ -e deploy/libs.sh ]; then . deploy/libs.sh; fi

echo_end "Configurando entorno ${LGREEN}${ENTORNO}${NC} de ${RED}${APP}${NC} ... "

echo_nend "Creando directorios de la aplicación ... "
mkdir -p ${WEBDIR}/${APP}/${APP} ${WEBDIR}/run ${WEBDIR}/log
chown -R ${USER_WEB}:${GROUP_WEB} ${WEBDIR}
chmod -R g+rw ${WEBDIR}
echo_listo

if [ -e deploy/upgrade_so.sh ]; then . deploy/upgrade_so.sh; fi

copia_archivos

if [[ $UPGRADE_APP == NO ]]; then

    echo_end "Configuración de la Base de datos de ${BLUE}${APP}${NC} ... "
    # Se crea el usuario y la base de datos dentro de postgres
    run_db_query "CREATE USER ${DB_USER} WITH PASSWORD '${DB_PASS}';"
    run_db_query "ALTER USER ${DB_USER} WITH PASSWORD '${DB_PASS}';"
    run_db_query "CREATE DATABASE ${DB_NAME};"
    run_db_query "GRANT ALL PRIVILEGES ON DATABASE ${DB_NAME} TO ${DB_USER};"
    echo_listo

    secret_config_db ${SECRET_FILE}
    secret_config ${SECRET_FILE}
fi

secret_key_config ${SETTINGS_FILE}

actualiza_prod

if [[ $UPGRADE_APP == NO ]]; then
    pushd ${DJANGODIR} >> /dev/null
    add_user ${USER_ADMIN} ${EMAIL_ADMIN} ${CLAVE_ADMIN}
    popd
fi

desactiva_servicio ${APP}
gunicorn_script
gunicorn_service
activa_servicio ${APP}

apache_conf
enable_apache

