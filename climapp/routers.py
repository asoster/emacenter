
from rest_framework.routers import DefaultRouter
from . import viewsets

router = DefaultRouter()

router.register('user', viewsets.UserViewSet, base_name='user')
router.register('station', viewsets.StationViewSet, base_name='station')
router.register('estacion', viewsets.StationViewSet, base_name='estacion')
router.register('data', viewsets.EstacionVista, base_name='data')
router.register('record', viewsets.RecordViewSet, base_name='record')
router.register('registro', viewsets.RecordViewSet, base_name='registro')
