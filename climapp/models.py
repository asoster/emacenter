# models.py
#
# Modelos de datos que se utilizan el la  aplicación
#
# LICENSE:  This file is part of EMA Libre.
# EMA Libre is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EMA Libre is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EMA Libre.  If not, see http://www.gnu.org/licenses/.
#
# @copyright  Copyright (c) 2019 GUGLER (http://www.gugler.com.ar)
# @license    https://www.gnu.org/licenses/gpl-3.0.html GPL License
# @link       https://www.gugler.com.ar
# @since      File available since Release v1.0.0


from django.db import models

import datetime

import uuid

from django.conf import settings
from django.urls import reverse
from django.core.exceptions import ValidationError

from django.utils import timezone
from django.utils.dateparse import parse_datetime
from django.utils.translation import ugettext_lazy as _

from django.db.models import Q, Min, Max
from django.db.models.signals import pre_save, post_save
from django.db.models.functions import TruncHour

from django.contrib.auth.models import User

from django.dispatch import receiver

from django.forms.models import model_to_dict

from emacenter.settings import DEBUG


''' Parámetros aceptados por el servidor de recolección de datos '''

post_request_bind = {
    'fecha': 'timestamp', 'temperatura_externa': 'decimal',
    'temperatura_interna': 'decimal',
    'humedad_externa': 'decimal', 'humedad_interna': 'decimal',
    'punto_de_rocio': 'decimal',
    'luxer_intencidad': 'decimal', 'luxer_uv': 'decimal',
    'viento_velocidad':  'decimal',
    'viento_rafagas': 'decimal', 'viento_direccion': 'decimal',
    'presion_absoluta': 'decimal',
    'presion_relativa': 'decimal', 'lloviendo': 'decimal',
    'lluvia_actual': 'decimal',
    'lluvia_acumulado_hora': 'decimal', 'lluvia_acumulado_diario': 'decimal',
    'software': 'string', 'tanque': 'bool',
    # ~ wunderground ***
    'dateutc': 'fecha', 'tempf': 'temperatura_externa',
    'indoortempf': 'temperatura_interna',
    'indoorhumidity': 'humedad_interna', 'humidity': 'humedad_externa',
    'dewptf': 'punto_de_rocio',
    'UV': 'luxer_uv', 'solarradiation': 'luxer_intencidad',
    'winddir': 'viento_direccion',
    'windgustmph': 'viento_rafagas', 'windspeedmph': 'viento_velocidad',
    'rainin': 'lluvia_actual',
    'dailyrainin': 'lluvia_acumulado_diario', 'baromin': 'presion_relativa',
    'softwaretype': 'software'
    # ~ ******************
}


class Estaciones(models.Model):
    """
    Clase para manejar los datos de las estaciones meteorológicas
    """
    id_estacion = models.AutoField(primary_key=True)
    id_usuario = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        null=True, blank=True, verbose_name=u'Usuario',
        related_name='estaciones', on_delete=models.SET_NULL)
    fecha_alta = models.DateTimeField(
        auto_now_add=True, verbose_name=u'Fecha alta')
    fecha_baja = models.DateTimeField(
        null=True, blank=True, verbose_name=u'Fecha baja')
    fecha_modificacion = models.DateTimeField(
        auto_now=True, null=True, blank=False)
    activa = models.BooleanField(verbose_name='Activa', default=True)
    clave_lectura = models.UUIDField(
        default=uuid.uuid4, editable=False,
        verbose_name=u'Clave de lectura')
    clave_escritura = models.UUIDField(
        default=uuid.uuid4, editable=False,
        verbose_name=u'Clave de escritura')
    modelo = models.CharField(
        null=False, blank=False, max_length=100,
        verbose_name=u'Modelo de la estación')
    nombre = models.CharField(
        null=True, blank=False, max_length=100,
        verbose_name=u'Nombre de la estación')
    pais = models.CharField(
        null=False, blank=False, max_length=100,
        verbose_name=u'País')
    ciudad = models.CharField(
        null=False, blank=False, max_length=100,
        verbose_name=u'Ciudad')
    calle = models.CharField(
        null=False, blank=False, max_length=100,
        verbose_name=u'Calle')
    latitud = models.DecimalField(
        u'Latitud', max_digits=10, decimal_places=6,
        default=0, null=True, blank=True)
    longitud = models.DecimalField(
        u'Longitud', max_digits=10, decimal_places=6,
        default=0, null=True, blank=True)
    altura = models.DecimalField(
        u'Altura sobre nivel del mar', max_digits=10,
        decimal_places=6, default=0, null=True, blank=True)
    fecha_ultimo_registro = models.DateTimeField(
        null=True, blank=True,
        verbose_name=u'Fecha de último registro cargado')
    lluvia_acumulado_hora = models.DecimalField(
        u'Lluvia acumulado hora',
        max_digits=10, decimal_places=2, default=0, null=True, blank=True)
    lluvia_acumulado_diario = models.DecimalField(
        u'Lluvia acumulado día',
        max_digits=10, decimal_places=2, default=0, null=True, blank=True)
    lluvia_acumulado_semanal = models.DecimalField(
        u'Lluvia acumulado semana',
        max_digits=10, decimal_places=2, default=0, null=True, blank=True)
    lluvia_acumulado_mensual = models.DecimalField(
        u'Lluvia acumulado mes',
        max_digits=10, decimal_places=2, default=0, null=True, blank=True)
    lluvia_acumulado_anual = models.DecimalField(
        u'Lluvia acumulado año',
        max_digits=10, decimal_places=2, default=0, null=True, blank=True)
    lluvia_acumulado_total = models.DecimalField(
        u'Lluvia acumulado total',
        max_digits=10, decimal_places=2, default=0, null=True, blank=True)

    @classmethod
    def filtrar_objetos(clase, busqueda):
        filtro = Q(activa=True)
        if busqueda:
            filtro = Q(nombre__contains=busqueda) \
                | Q(modelo__contains=busqueda) \
                | Q(pais__contains=busqueda) \
                | Q(ciudad__contains=busqueda) | Q(calle__contains=busqueda)
        return clase.objects.filter(Q(activa=True)).filter(filtro)

    def __str__(self):
        return '{} ({}, {})'.format(self.nombre, self.pais, self.ciudad)

    def __unicode__(self):
        return u'{} ({}, {})'.format(self.nombre, self.pais, self.ciudad)

    def modelo_estacion(self):
        return '{}'.format(self.modelo)

    def get_absolute_url(self):
        return reverse('station_detail', kwargs={'pk': self.pk})

    def to_dict(self, cantidad_registros=5):
        datos = model_to_dict(self, fields=(
            'nombre', 'modelo', 'pais', 'ciudad', 'latitud', 'longitud'))
        datos['registros'] = [
            reg.to_dict()
            for reg in self.ultimos_registros(cantidad_registros)]
        datos['estadisticas'] = list(self.get_estadisticas())
        return datos

    def ultimo_registro(self):
        result = self.registros.order_by('-fecha')[:1]
        if len(result) == 0:
            return False
        return result[0]

    def ultimos_registros(self, cantidad=5):
        return self.registros.order_by('-fecha')[:cantidad]

    def get_estadisticas(self, cantidad_dias=1):
        return self.registros.filter(fecha__gte=(
                timezone.now() - datetime.timedelta(days=cantidad_dias))
           ).annotate(hora=TruncHour(
                'fecha', tzinfo=timezone.get_current_timezone())
            ).values('hora').annotate(
                temperatura_externa_max=Max('temperatura_externa'),
                humedad_externa_max=Max('humedad_externa'),
                viento_velocidad_max=Max('viento_velocidad'),
                luxer_intencidad_max=Max('luxer_intencidad'),
                luxer_uv_max=Max('luxer_uv'),
                presion_absoluta_max=Max('presion_absoluta'),
                presion_relativa_max=Max('presion_relativa'),
                lluvia_acumulado_hora_max=Max('lluvia_acumulado_hora'),
                viento_rafagas_max=Max('viento_rafagas'),
                lluvia_acumulado_hora_min=Min('lluvia_acumulado_hora'),
                temperatura_externa_min=Min('temperatura_externa'),
                humedad_externa_min=Min('humedad_externa'),
                viento_velocidad_min=Min('viento_velocidad'),
                luxer_intencidad_min=Min('luxer_intencidad'),
                luxer_uv_min=Min('luxer_uv'),
                presion_absoluta_min=Min('presion_absoluta'),
                presion_relativa_min=Min('presion_relativa')
           ).order_by('-hora')

    def fecha(self):
        return self.fecha_alta
    fecha.short_description = _(u'Fecha')

    def estado(self):
        if self.fecha_ultimo_registro is None or \
            (timezone.now() - self.fecha_ultimo_registro).total_seconds() \
                / 60.0 > 30:
            return u'Fuera de línea'
        return u'En línea'
    estado.short_description = _(u'Estado de la estación')

    def get_nombre_lista(self):
        return '{}'.format(self.nombre)
    get_nombre_lista.short_description = _(u'Nombre')

    def activar(self):
        if not self.activa:
            self.activa = True
            self.save()

    def desactivar(self):
        if self.activa:
            self.activa = False
            self.save()

    def regenerar_clave_lectura(self):
        self.clave_lectura = uuid.uuid4()
        self.save()
    regenerar_clave_lectura.short_description = \
        _(u'Regenerar clave de lectura')

    def regenerar_clave_escritura(self):
        self.clave_escritura = uuid.uuid4()
        self.save()
    regenerar_clave_escritura.short_description = \
        _(u'Regenerar clave de escritura')

    class Meta:
        verbose_name = _('Estación meteorológica')
        verbose_name_plural = _('Estaciones meteorológicas')
        ordering = ['pais', 'ciudad', 'modelo']


class Registros(models.Model):
    """
    Clase para manejar los datos de los registros de las estaciones
    meteorológicas
    """

    id_registro = models.AutoField(primary_key=True)
    id_estacion = models.ForeignKey(
        Estaciones, verbose_name='Estación',
        related_name='registros', on_delete=models.CASCADE)
    fecha = models.DateTimeField(default=timezone.now, verbose_name=u'Fecha')
    fecha_ingreso = models.DateTimeField(
        auto_now_add=True, verbose_name=u'Fecha ingreso')

    fecha_alta = models.DateTimeField(
        auto_now_add=True, verbose_name=u'Fecha alta')
    fecha_baja = models.DateTimeField(
        null=True, blank=True, verbose_name=u'Fecha baja')
    software = models.CharField(
        null=False, blank=False, max_length=100, verbose_name=u'Software')
    viento_direccion_nombre = models.CharField(
        null=False, blank=False, max_length=100,
        verbose_name=u'Dirección del viento')

    temperatura_externa = models.DecimalField(
        u'Temperatura externa',
        max_digits=10, decimal_places=2,
        default=0, null=True, blank=True)
    temperatura_interna = models.DecimalField(
        u'Temperatura interna',
        max_digits=10, decimal_places=2,
        default=0, null=True, blank=True)
    humedad_externa = models.DecimalField(
        u'Humedad externa', max_digits=10,
        decimal_places=2,
        default=0, null=True, blank=True)
    humedad_interna = models.DecimalField(
        u'Humedad interna', max_digits=10,
        decimal_places=2,
        default=0, null=True, blank=True)
    punto_de_rocio = models.DecimalField(
        u'Punto de rocío', max_digits=10,
        decimal_places=2,
        default=0, null=True, blank=True)
    luxer_intencidad = models.DecimalField(
        u'Intencidad Luxer', max_digits=10,
        decimal_places=2,
        default=0, null=True, blank=True)
    luxer_uv = models.DecimalField(
        u'luxer_uv', max_digits=10, decimal_places=2,
        default=0, null=True, blank=True)
    viento_velocidad = models.DecimalField(
        u'Velocidad del viento',
        max_digits=10, decimal_places=2,
        default=0, null=True, blank=True)
    viento_rafagas = models.DecimalField(
        u'Ráfagas de viento', max_digits=10,
        decimal_places=2,
        default=0, null=True, blank=True)
    viento_direccion = models.DecimalField(
        u'Dirección del viento',
        max_digits=10, decimal_places=2,
        default=0, null=True, blank=True)
    presion_absoluta = models.DecimalField(
        u'Presión absoluta', max_digits=10,
        decimal_places=2,
        default=0, null=True, blank=True)
    presion_relativa = models.DecimalField(
        u'Presión relativa', max_digits=10,
        decimal_places=2,
        default=0, null=True, blank=True)

    lluvia_actual = models.DecimalField(
        u'Lluvia actual', max_digits=10,
        decimal_places=2,
        default=0, null=True, blank=True)
    lluvia_acumulado_hora = models.DecimalField(
        u'Lluvia acumulado hora',
        max_digits=10, decimal_places=2,
        default=0, null=True, blank=True)
    lluvia_acumulado_diario = models.DecimalField(
        u'Lluvia acumulado día',
        max_digits=10, decimal_places=2,
        default=0, null=True, blank=True)
    lluvia_acumulado_semanal = models.DecimalField(
        u'Lluvia acumulado semana',
        max_digits=10, decimal_places=2,
        default=0, null=True, blank=True)
    lluvia_acumulado_mensual = models.DecimalField(
        u'Lluvia acumulado mes',
        max_digits=10, decimal_places=2,
        default=0, null=True, blank=True)
    lluvia_acumulado_anual = models.DecimalField(
        u'Lluvia acumulado año',
        max_digits=10, decimal_places=2,
        default=0, null=True, blank=True)
    lluvia_acumulado_total = models.DecimalField(
        u'Lluvia acumulado total',
        max_digits=10, decimal_places=2,
        default=0, null=True, blank=True)

    sin_acumulado_lluvia = models.NullBooleanField(
        u'Registro sin acumulado de lluvia',
        null=True, blank=True)

    tanque = models.BooleanField(
        'Registro con atnque en origen',
        null=True, blank=True)

    def __str__(self):
        return '{} {}'.format(self.id_estacion.modelo, self.fecha)

    def __unicode__(self):
        return u'{} {}'.format(self.id_estacion.modelo, self.fecha)

    def modelo_estacion(self):
        return '{}'.format(self.id_estacion.modelo)

    def get_nombre_lista(self):
        return '{}'.format(self.id_estacion)
    get_nombre_lista.short_description = _(u'Nombre')

    class Meta:
        verbose_name = _('Registro de estación meteorológica')
        verbose_name_plural = _('Registros de estación meteorológica')
        ordering = ['-fecha_alta', 'id_estacion']

    def to_dict(self):
        datos = model_to_dict(self, fields=(
            'fecha', 'software', 'temperatura_externa', 'temperatura_interna',
            'humedad_externa', 'humedad_interna', 'punto_de_rocio',
            'luxer_intencidad', 'luxer_uv', 'tanque',
            'viento_velocidad', 'viento_rafagas', 'viento_direccion',
            'viento_direccion_nombre', 'presion_absoluta', 'presion_relativa',
            'lloviendo', 'lluvia_actual', 'lluvia_acumulado_hora',
            'lluvia_acumulado_diario', 'lluvia_acumulado_semanal',
            'lluvia_acumulado_mensual', 'lluvia_acumulado_anual',
            'lluvia_acumulado_total'))
        for attr in datos:
            if isinstance(datos[attr], datetime.datetime):
                datos[attr] = datos[attr].astimezone().isoformat()
        return datos

    def cargar_datos_desde_dict(self, datos):
        for k, v in post_request_bind.items():
            if k not in datos:
                continue
            clave = k
            if v in post_request_bind:
                clave = post_request_bind.get(v, '')
            tipo = post_request_bind.get(clave, None)
            dato = None
            if not tipo:
                continue
            if tipo in ['datetime', 'date', 'timestamp']:
                dato = parse_datetime(str(datos.get(k)).replace(',', '.'))
                if timezone.is_naive(dato):
                    dato = timezone.make_aware(dato)
            elif tipo in ['decimal', 'number', 'float']:
                dato = float(str(datos.get(k)).replace(',', '.'))
            elif tipo in ['string', 'cadena']:
                dato = datos.get(k, '-')
                if clave == 'software' and datos.get('software', '-') == '-':
                    dato = 'desconocido'

            if dato and clave in datos:
                self.__setattr__(clave, dato)

    def nombre_viento(self):
        if self.viento_direccion == 0:
            return 'Norte'
        if self.viento_direccion < 45:
            return 'NorNorEste'
        if self.viento_direccion == 45:
            return 'NorEste'
        if self.viento_direccion < 90:
            return 'EsteNorEste'
        if self.viento_direccion == 90:
            return 'Este'
        if self.viento_direccion < 135:
            return 'EsteSurEste'
        if self.viento_direccion == 135:
            return 'SurEste'
        if self.viento_direccion < 180:
            return 'SurSurEste'
        if self.viento_direccion == 180:
            return 'Sur'
        if self.viento_direccion < 225:
            return 'SurSurOeste'
        if self.viento_direccion == 225:
            return 'SurOeste'
        if self.viento_direccion < 270:
            return 'OesteSurOeste'
        if self.viento_direccion == 270:
            return 'Oeste'
        if self.viento_direccion < 315:
            return 'OesteNorOeste'
        if self.viento_direccion == 315:
            return 'NorOeste'
        if self.viento_direccion < 360:
            return 'NorNorOeste'
        return 'N/A'

    def save(self, *args, **kwargs):
        self.clean()
        return super().save(*args, **kwargs)

    campos_validacion_0 = [
        'temperatura_externa', 'temperatura_interna', 'luxer_uv',
        'viento_direccion', 'humedad_interna', 'humedad_externa']

    def is_valid_nivel0(self):
        return sum([getattr(self, c) for c in self.campos_validacion_0]) > 0

    campos_validacion_1 = {
        'humedad_externa': {'min': 0, 'max': 100},
        'humedad_interna': {'min': 0, 'max': 100},
        'luxer_uv': {'min': 0, 'max': 10},
        'temperatura_externa': {'min': 0, 'max': 60},
        'temperatura_interna': {'min': 0, 'max': 60},
        'viento_direccion': {'min': 0, 'max': 360}}

    def is_valid_nivel1(self):
        valid = True
        result = {}
        for c in self.campos_validacion_1.keys():
            if not(getattr(self, c) >= self.campos_validacion_1[c]['min'] and
                    getattr(self, c) <= self.campos_validacion_1[c]['max']):
                result[c] = 'El valor de {} debe estar entre {} y {}'.format(
                        self._meta.get_field(c).verbose_name,
                        self.campos_validacion_1[c]['min'],
                        self.campos_validacion_1[c]['max']
                   )
                valid = False
        return valid, result

    def clean(self, *args, **kwargs):
        error = {}
        self.viento_direccion_nombre = self.nombre_viento()

        if self.fecha is None:
            self.fecha = timezone.now()

        # Se controla que los registros no sean de fecha futura.
        # Sólo permite que ingresen fechas futuras si se está haciendo debug de
        # la aplicación.
        if not DEBUG and self.fecha.astimezone() > timezone.now().astimezone():
            error['fecha_registro'] = u'La fecha del registro enviado está ' \
                'en el futuro'

        if not self.is_valid_nivel0():
            for c in self.campos_validacion_0:
                error[c] = 'No hay suficientes datos para guardar registro'

        valid1 = self.is_valid_nivel1()
        if not valid1[0]:
            error.update(valid1[1])

        if len(error.keys()) > 0:
            raise ValidationError(error)

        super(Registros, self).clean(*args, **kwargs)

    def procesa_acumulado_lluvia(self):
        if self.fecha is None:
            self.fecha = timezone.now()

        if self.sin_acumulado_lluvia is not None:
            return None

        self.sin_acumulado_lluvia = True

        # Si el registro que se envía tiene una antigüedad de mas de 1 minuto,
        # no genera datos acumulados
        if self.fecha.astimezone() < (
                timezone.now() - datetime.timedelta(minutes=1)).astimezone():
            return None

        if self.id_estacion.fecha_ultimo_registro is None:
            self.id_estacion.fecha_ultimo_registro = timezone.now() -  \
                datetime.timedelta(minutes=1)
            self.id_estacion.lluvia_acumulado_hora = 0
            self.id_estacion.lluvia_acumulado_diario = 0
            self.id_estacion.lluvia_acumulado_semanal = 0
            self.id_estacion.lluvia_acumulado_mensual = 0
            self.id_estacion.lluvia_acumulado_anual = 0
            self.id_estacion.lluvia_acumulado_total = 0

        if self.fecha.astimezone() > \
                self.id_estacion.fecha_ultimo_registro.astimezone():

            if self.fecha.astimezone().isocalendar() != self.id_estacion. \
                    fecha_ultimo_registro.astimezone().isocalendar():

                if self.fecha.astimezone().year != self.id_estacion. \
                        fecha_ultimo_registro.astimezone().year:
                    self.id_estacion.lluvia_acumulado_anual = 0
                if self.fecha.astimezone().month != self.id_estacion. \
                        fecha_ultimo_registro.astimezone().month:
                    self.id_estacion.lluvia_acumulado_mensual = 0
                if self.fecha.astimezone().isocalendar()[1] != \
                        self.id_estacion.fecha_ultimo_registro.astimezone() \
                        .isocalendar()[1]:
                    self.id_estacion.lluvia_acumulado_semanal = 0
                if self.fecha.astimezone().weekday() == 0:
                    self.id_estacion.lluvia_acumulado_semanal = 0

                self.id_estacion.lluvia_acumulado_total += \
                    self.id_estacion.lluvia_acumulado_diario
                self.id_estacion.lluvia_acumulado_anual += \
                    self.id_estacion.lluvia_acumulado_diario
                self.id_estacion.lluvia_acumulado_mensual += \
                    self.id_estacion.lluvia_acumulado_diario
                self.id_estacion.lluvia_acumulado_semanal += \
                    self.id_estacion.lluvia_acumulado_diario

                self.id_estacion.lluvia_acumulado_diario = 0
                self.id_estacion.lluvia_acumulado_hora = 0

            if self.fecha.astimezone().hour != \
                    self.id_estacion.fecha_ultimo_registro.astimezone().hour:
                self.id_estacion.lluvia_acumulado_hora = 0

            if self.lluvia_acumulado_hora > \
                    self.id_estacion.lluvia_acumulado_hora:
                self.id_estacion.lluvia_acumulado_hora = \
                    self.lluvia_acumulado_hora

            if self.lluvia_acumulado_diario > \
                    self.id_estacion.lluvia_acumulado_diario:
                self.id_estacion.lluvia_acumulado_diario = \
                    self.lluvia_acumulado_diario

            self.id_estacion.fecha_ultimo_registro = self.fecha.astimezone()
            self.id_estacion.save()

            self.lluvia_acumulado_semanal = \
                self.id_estacion.lluvia_acumulado_semanal
            self.lluvia_acumulado_mensual = \
                self.id_estacion.lluvia_acumulado_mensual
            self.lluvia_acumulado_anual = \
                self.id_estacion.lluvia_acumulado_anual
            self.lluvia_acumulado_total = \
                self.id_estacion.lluvia_acumulado_total
            '''
            Se normaliza el registro actual con los datos de la última
            actualización, para los casos en que el cliente pierda el
            acumulado de la última hora o día.
            '''
            self.lluvia_acumulado_diario = \
                self.id_estacion.lluvia_acumulado_diario
            self.lluvia_acumulado_hora = \
                self.id_estacion.lluvia_acumulado_hora

            self.sin_acumulado_lluvia = False


class Calidad(models.Model):
    """
    Manejo de los datos de calidad de los registros de las estaciones
    meteorológicas
    """

    id = models.AutoField(primary_key=True)
    registro = models.ForeignKey(
        Registros, verbose_name='Registro',
        null=False, blank=False,
        related_name='calidad', on_delete=models.CASCADE)
    observacion = models.CharField(
        null=True, blank=True, max_length=100,
        verbose_name='Observaciones del registro')
    tanque = models.BooleanField('Registro con tanque en origen',
                                 null=True, blank=True)
    nivel0 = models.BooleanField('Nivel 0 de calidad', null=True, blank=True)
    nivel1 = models.BooleanField('Nivel 1 de calidad', null=True, blank=True)
    nivel2 = models.BooleanField('Nivel 2 de calidad', null=True, blank=True)
    nivel3 = models.BooleanField('Nivel 3 de calidad', null=True, blank=True)
    nivel4 = models.BooleanField('Nivel 4 de calidad', null=True, blank=True)
    nivel5 = models.BooleanField('Nivel 5 de calidad', null=True, blank=True)
    nivel6 = models.BooleanField('Nivel 6 de calidad', null=True, blank=True)

    class Meta:
        verbose_name = _('Calidad del registro')
        verbose_name_plural = _('Calidad de los registros')
        ordering = ['-id']


@receiver(pre_save, sender=Registros)
def registros_pre_save(sender, instance, *args, **kwargs):
    if instance.sin_acumulado_lluvia is None:
        instance.procesa_acumulado_lluvia()


@receiver(post_save, sender=Registros)
def registros_post_save(sender, instance, created, **kwargs):
    if created:
        c = Calidad(registro=instance, tanque=instance.tanque)
        c.nivel0 = instance.is_valid_nivel0()
        c.nivel1 = instance.is_valid_nivel1()[0]
        c.save()


@receiver(post_save, sender=User)
def create_or_update_user_profile(sender, instance, created, **kwargs):
    if created:
        instance.is_staff = True
        instance.save()
