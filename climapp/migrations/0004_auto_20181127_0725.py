# Generated by Django 2.0 on 2018-11-27 10:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('climapp', '0003_registros_sin_acumulado_lluvia'),
    ]

    operations = [
        migrations.AlterField(
            model_name='registros',
            name='sin_acumulado_lluvia',
            field=models.NullBooleanField(verbose_name='Registro sin acumulado de lluvia'),
        ),
    ]
