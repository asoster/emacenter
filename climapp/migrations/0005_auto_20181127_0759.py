# Generated by Django 2.0 on 2018-11-27 10:59

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('climapp', '0004_auto_20181127_0725'),
    ]

    operations = [
        migrations.AlterField(
            model_name='registros',
            name='fecha',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='Fecha'),
        ),
        migrations.AlterField(
            model_name='registros',
            name='fecha_alta',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Fecha alta'),
        ),
        migrations.AlterField(
            model_name='registros',
            name='fecha_baja',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Fecha baja'),
        ),
        migrations.AlterField(
            model_name='registros',
            name='fecha_ingreso',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Fecha ingreso'),
        ),
    ]
