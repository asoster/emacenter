# Generated by Django 2.0 on 2018-09-28 01:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('climapp', '0002_estaciones_nombre'),
    ]

    operations = [
        migrations.AddField(
            model_name='registros',
            name='sin_acumulado_lluvia',
            field=models.NullBooleanField(verbose_name='Registro que no genera acumulado de lluvia'),
        ),
    ]
