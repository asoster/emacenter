# Generated by Django 2.2.4 on 2021-03-14 23:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('climapp', '0009_auto_20210314_1953'),
    ]

    operations = [
        migrations.AlterField(
            model_name='calidad',
            name='registro',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='calidad', to='climapp.Registros', verbose_name='Registro'),
        ),
    ]
