# viewsets.py
#
# Funciones para  generar los resultados que se devuelve al cliente de la
# aplicación
#
# LICENSE:  This file is part of EMA Libre.
# EMA Libre is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EMA Libre is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EMA Libre.  If not, see http: //www.gnu.org/licenses/.
#
# @copyright  Copyright (c) 2019 GUGLER (http: //www.gugler.com.ar)
# @license    https: //www.gnu.org/licenses/gpl-3.0.html GPL License
# @link       https: //www.gugler.com.ar
# @since      File available since Release v1.0.0


from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404

from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.decorators import action

from .serializers import (
    UserRetrieveSerializer,
    UserSerializer,
    EstacionSerial,
    EstacionListSerial,
    RegistrosSerial,
    EstadisticasSerializer
)
from .models import Estaciones, Registros


class UserViewSet(ReadOnlyModelViewSet):
    '''
    list:
    Genera una lista de todos los usuarios del aplicativo.

    retrieve:
    Genera la información detallada de un usuario del aplicativo.
    '''
    queryset = User.objects.all()
    serializer_class = UserRetrieveSerializer
    serializer_class_list = UserSerializer

    def get_permissions(self):
        if self.action in ('list', 'retrieve'):
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]

    def list(self, request):
        queryset = self.queryset
        search = request.query_params.get('q', None)
        if search is not None:
            queryset = queryset.filter(username__contains=search)
        if not request.user.is_superuser:
            queryset = queryset.filter(pk=request.user.id)
        serializer = self.serializer_class_list(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        user = get_object_or_404(self.queryset, pk=pk)
        if not request.user.is_superuser and int(pk) != request.user.id:
            user = get_object_or_404(self.queryset, pk=-1)
        serializer = self.serializer_class(user)
        return Response(serializer.data)


class StationViewSet(ReadOnlyModelViewSet):
    '''
    list:
    Genera una lista de todas las estaciones del aplicativo.

    retrieve:
    Genera la información detallada de una estación del aplicativo.

    registros:
    Genera una lista de los últimos registros de una estación meteorológica.

    estadisticas:
    Genera una lista con las estadísticas del último día registros de una
    estación meteorológica.

    '''
    queryset = Estaciones.objects.all()
    serializer_class = EstacionSerial
    serializer_class_list = EstacionListSerial

    def get_queryset(self):
        busqueda = self.request.GET.get('q', '')
        return Estaciones.filtrar_objetos(busqueda)

    def list(self, request):
        serializer = self.serializer_class_list(self.get_queryset(), many=True)
        return Response(serializer.data)

    @action(detail=True, methods=['get'], permission_classes=[])
    def registros(self, request, pk=None):
        '''
        Lista los últimos 15 registros de la estación
        '''
        r = self.get_object().registros.order_by('-fecha')[: 15]
        serializer = RegistrosSerial(r, many=True)
        return Response(serializer.data)

    @action(detail=True, methods=['get'], permission_classes=[])
    def estadisticas(self, request, pk=None):
        r = self.get_object().get_estadisticas()
        serializer = EstadisticasSerializer(r, many=True)
        return Response(serializer.data)


class EstacionVista(StationViewSet):
    '''
    list:
    Genera una lista de todas las estaciones del aplicativo.

    retrieve:
    Genera la información detallada de una estación del aplicativo accediendo
    mediante la clave de lectura.
    '''
    lookup_field = 'clave_lectura'

    def get_permissions(self):
        if self.action is not None and \
                self.action in ('get', 'retrieve', 'record'):
            permission_classes = []
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]

    @action(detail=True, methods=['get'], permission_classes=[])
    def record(self, request, clave_lectura=None):
        '''
        Lista los últimos 15 registros de la estación
        '''
        r = self.get_object().registros.order_by('-fecha')[: 15]
        serializer = RegistrosSerial(r, many=True)
        return Response(serializer.data)


class RecordViewSet(ReadOnlyModelViewSet):
    '''
    list:
    Genera una lista de todas los registros de la estación registrada del
    usuario.

    retrieve:
    Genera la información detallada de un registro de estación del aplicativo.
    '''
    queryset = Registros.objects.all()
    serializer_class = RegistrosSerial
    serializer_class_list = RegistrosSerial

    def get_permissions(self):
        if self.action in ('list', 'retrieve'):
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]

    def list(self, request):
        serializer = self.serializer_class_list(
                self.queryset.filter(
                        id_estacion__id_usuario=request.user)
                    .order_by('-fecha')[: 50], many=True)
        return Response(serializer.data)
