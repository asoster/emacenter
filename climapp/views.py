# views.py
#
# Funciones para  generar los resultados que se devuelve al cliente de la
# aplicación
#
# LICENSE:  This file is part of EMA Libre.
# EMA Libre is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EMA Libre is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EMA Libre.  If not, see http: //www.gnu.org/licenses/.
#
# @copyright  Copyright (c) 2019 GUGLER (http: //www.gugler.com.ar)
# @license    https: //www.gnu.org/licenses/gpl-3.0.html GPL License
# @link       https: //www.gugler.com.ar
# @since      File available since Release v1.0.0


from __future__ import absolute_import

from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.core.mail import EmailMessage

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.utils.html import strip_tags
from django.http import HttpResponse, JsonResponse
from django.views import View
from django.template.loader import render_to_string
from django.views.generic import (
    TemplateView,
    ListView,
    DetailView
)
from django.views.generic.edit import (
    FormView,
    CreateView,
    UpdateView
)
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from django.db.models import Count, Q, F
from django.db.models.functions import Extract

import traceback

from . import app_title

from emacenter import settings
from .forms import UserForm, ContactForm, EstacionesForm
from .decorators import ContextoVista, LoginRequired
from .models import Estaciones, Registros, post_request_bind


class MailOfflineView(ContextoVista, View):
    template_offline = 'mail/station_offline.html'
    template_online = 'mail/station_online.html'
    template_ranking = 'mail/station_ranking.html'
    cantidad_horas = 2

    def send_email_offline(self, request, estaciones):
        for e in estaciones:
            send_email(
                body=render_to_string(
                        self.template_offline,
                        {'object': e, 'horas': self.cantidad_horas}),
                subject='{} - Estación Fuera de Línea : ('.format(
                            get_current_site(request).name),
                to_email=[e.id_usuario.email]
            )

    def send_email_online(self, request, estaciones):
        for e in estaciones:
            send_email(
                body=render_to_string(
                    self.template_online,
                    {'object': e, 'horas': self.cantidad_horas}),
                subject='{} - Estación En Línea : )'.format(
                        get_current_site(request).name
                    ),
                to_email=[e.id_usuario.email]
            )

    def send_email_ranking(self, request, estaciones):
        send_email(
            body=render_to_string(
                self.template_ranking, {'object': estaciones}),
            subject='{} - Ranking Uptime día {: %Y-%m-%d}'.format(
                    get_current_site(request).name,
                    timezone.now() - timezone.timedelta(days=1)),
            to_email=[m[1] for m in settings.MANAGERS]
        )

    def ranking_queryset(self):
        fecha = timezone.now() - timezone.timedelta(days=1)

        result = Registros.objects.filter(
                id_estacion__activa=True, fecha__date=fecha.date()
            ).annotate(
                hora=Extract('fecha', 'hour'),
                minuto=Extract('fecha', 'minute'),
                cantidad_con_tanque=Count(
                    'pk', filter=Q(fecha_ingreso__gt=F('fecha') +
                                   timezone.timedelta(seconds=3))),
                cantidad_sin_tanque=Count(
                    'pk', filter=Q(fecha_ingreso__lte=F('fecha') +
                                   timezone.timedelta(seconds=3))
                )
            ).values(
                'hora', 'minuto', 'id_estacion', 'id_estacion__nombre',
                'id_estacion__id_usuario__username',
                'id_estacion__id_usuario__email',
                'cantidad_sin_tanque', 'cantidad_con_tanque',
                'id_estacion__ciudad', 'id_estacion__calle',
                'id_estacion__pais').annotate(
                cantidad_registros=Count('pk')
            ).order_by('hora', 'minuto', '-cantidad_con_tanque')

        datos = {}
        for d in result:
            clave = '{}'.format(d['id_estacion'])
            if clave not in datos:
                datos[clave] = d
                datos[clave]['dia'] = fecha
                datos[clave]['cantidad_minutos'] = 0
                datos[clave]['cantidad_minutos_sin_tanque'] = 0
                datos[clave]['cantidad_minutos_con_tanque'] = 0
            else:
                datos[clave]['cantidad_registros'] += d['cantidad_registros']
                datos[clave]['cantidad_sin_tanque'] += d['cantidad_sin_tanque']
                datos[clave]['cantidad_con_tanque'] += d['cantidad_con_tanque']
            datos[clave]['cantidad_minutos'] += 1
            if int(d['cantidad_con_tanque']) > 0:
                datos[clave]['cantidad_minutos_con_tanque'] += 1
            datos[clave]['uptime_con_tanque'] = \
                datos[clave]['cantidad_minutos_con_tanque'] * 100 / 1440
            if int(d['cantidad_sin_tanque']) > 0:
                datos[clave]['cantidad_minutos_sin_tanque'] += 1
            datos[clave]['uptime_sin_tanque'] = \
                datos[clave]['cantidad_minutos_sin_tanque'] * 100 / 1440

        return sorted(
                datos.values(),
                key=lambda x: x['cantidad_minutos_con_tanque'],
                reverse=True)

    def offline_queryset(self):
        return Estaciones.objects.filter(
                fecha_ultimo_registro__gte=(
                    timezone.now() -
                    timezone.timedelta(hours=1+self.cantidad_horas))
                ).filter(
                fecha_ultimo_registro__lte=(
                    timezone.now() -
                    timezone.timedelta(hours=self.cantidad_horas))
                ).exclude(id_usuario__email='')

    def online_queryset(self):
        return Estaciones.objects.filter(
                    fecha_ultimo_registro__gte=(
                        timezone.now() - timezone.timedelta(hours=1))
                ).exclude(id_usuario__email='').exclude(
                    registros__fecha__range=(
                        timezone.now() -
                        timezone.timedelta(hours=1+self.cantidad_horas),
                        timezone.now() - timezone.timedelta(hours=1)
                    )
                )

    def get(self, request, tipo='offline'):
        enviar_mail = request.GET.get('enviar', 'si') == 'si'
        enviar_html = request.GET.get('html', 'no') == 'si'
        tipo_mail = request.GET.get('tipo', tipo)
        clave_mail = request.GET.get('clave_mail', '')
        mensaje = {'mensaje': 'No hay nada por hacer'}
        est = []
        fx_correo = self.send_email_offline
        template_html = self.template_offline
        objeto = None

        if not (clave_mail and clave_mail == settings.CLAVE_MAIL_OFFLINE):
            tipo_mail = 'no_enviar'

        if tipo_mail == 'offline':
            est = self.offline_queryset()
            fx_correo = self.send_email_offline
            template_html = self.template_offline
            if len(est) > 0:
                objeto = est[0]

        if tipo_mail == 'online':
            est = self.online_queryset()
            fx_correo = self.send_email_online
            template_html = self.template_online
            if len(est) > 0:
                objeto = est[0]

        if tipo_mail == 'ranking':
            est = self.ranking_queryset()
            fx_correo = self.send_email_ranking
            template_html = self.template_ranking
            if len(est) > 0:
                objeto = est

        if len(est) == 0:
            mensaje = {'mensaje': 'No hay datos para mostrar'}
        else:
            mensaje = {
                'mensaje': 'Se ha enviado correo de '
                           'estaciones {}'.format(tipo),
                'cantidad': len(est)}
            if enviar_mail and not enviar_html:
                fx_correo(request, est)
            elif enviar_html:
                return HttpResponse(
                            render_to_string(template_html, {'object': objeto})
                        )

        return JsonResponse(mensaje, status=200)


class HomePageView(ContextoVista, TemplateView):
    '''
    Clase para renderizar la página principal de la aplicación, mediante un
    template html.
    '''
    template_name = 'home.html'


class UserInfoPageView(LoginRequired, ContextoVista, TemplateView):
    '''
    Clase para renderizar la página con la información del usuario de la
    aplicación, mediante un template html.
    '''
    template_name = 'auth/user_info.html'
    contexto = {
        'titulo': _(u'Información del usuario'),
        'mensaje': _(u'Detalle del usuario: '),
    }


class UpdateProfile(LoginRequired, ContextoVista, UpdateView):
    '''
    Clase para renderizar el formulario para actualizar la información del
    usuario de la aplicación mediante un template html.
    '''
    form_class = UserForm
    model = User
    slug_field = 'username'
    success_url = reverse_lazy('user_info')

    def dispatch(self, request, *args, **kwargs):
        obj = self.get_object()
        if obj.id != self.request.user.id:
            return redirect(reverse_lazy(
                                'user_update',
                                args=[self.request.user.username]))
        return super(self.__class__, self).dispatch(request, *args, **kwargs)


class ContactView(FormView):
    '''
    Clase para renderizar el formulario para ingresar datos de contacto para un
    usuario de la aplicación, mediante un template html.
    '''
    template_name = 'contact.html'
    form_class = ContactForm
    success_url = reverse_lazy('contact_thanks')

    def send_email(self, name, from_email, message):
        send_email(
            body='{}, \nhemos recibido su mensaje de contacto.\n'
                 'En breve nos comunicaremos.\n'
                 'Saludos\n\n---\n{}'.format(name, message),
            subject='{} : : Formulario de contacto'.format(app_title),
            to_email=from_email)

    def form_valid(self, form):
        self.send_email(
            form.cleaned_data['name'], form.cleaned_data['email'],
            form.cleaned_data['message'])
        return super(ContactView, self).form_valid(form)


class GraciasView(ContextoVista, TemplateView):
    '''
    Clase para renderizar el mensaje de agradecimiento para los usuarios que
    realizaron una consulta por medio del formulario de contacto, mediante
    un template html.
    '''
    template_name = 'message.html'
    contexto = {
        'titulo': _(u'Gracias por su contacto'),
        'mensaje': _(u'Muchas gracias por su consulta!!!\n'
                     'En breve nos estaremos comunicando con Ud.')}


class StationListView(ContextoVista, ListView):
    '''
    Clase para renderizar el listado de estaciones disponibles, mediante un
    template html.
    '''
    template_name = 'station_list.html'
    context_object_name = 'station_list'
    contexto = {'form_buscar': True}

    def get_context_data(self, **kwargs):
        context = super(StationListView, self).get_context_data(**kwargs)
        context['input'] = self.request.GET.get("q")
        return context

    def get_queryset(self):
        '''
        Devuelve la lista de estaciones activas y los filtra con el campo
        de búsqueda (q) que puede ser enviado por parámetro en el GET.
        '''
        busqueda = self.request.GET.get('q', '')
        self.contexto['q'] = busqueda
        return Estaciones.filtrar_objetos(busqueda)


class StationDetailView(ContextoVista, DetailView):
    '''
    Clase para renderizar el detalle de la información de las estaciones
    disponibles, mediante un template html.
    '''
    model = Estaciones
    context_object_name = 'station'
    template_name = 'station_detail.html'


class EstacionCreate(LoginRequired, ContextoVista, CreateView):
    '''
    Clase que permite el registro de los datos de una nueva estación
    meteorológica y los del usuario dueño de la misma, mediante un template
    html.
    '''
    model = Estaciones
    fields = ['nombre', 'modelo', 'pais', 'ciudad', 'calle',
              'latitud', 'longitud', 'altura']
    success_url = reverse_lazy('station_thanks')

    def send_email(self, user):
        send_email(
            body='{} {}, \nha registrado correctamente la estación.\n'
                 'Gracias'.format(user.first_name, user.last_name),
            subject='{} : : Registro de estación'.format(app_title),
            to_email=user.email)

    def form_valid(self, form):
        form.instance.id_usuario = self.request.user
        self.send_email(self.request.user)
        return super(EstacionCreate, self).form_valid(form)


class EstacionUpdate(LoginRequired, ContextoVista, UpdateView):
    '''
    Clase para renderizar el formulario para actualizar los datos de las
    estaciones de la aplicación, mediante un template html.
    '''
    form_class = EstacionesForm
    model = Estaciones

    def dispatch(self, request, *args, **kwargs):
        obj = self.get_object()
        if obj.id_usuario != self.request.user:
            return redirect(obj)
        return super(self.__class__, self).dispatch(request, *args, **kwargs)


class StationGraciasView(ContextoVista, TemplateView):
    '''
    Clase para renderizar el mensaje de agradecimiento para los usuarios que
    realizaron un registro de una estación meteorológica, mediante un
    template html.
    '''
    template_name = 'message.html'
    contexto = {
        'titulo': _(u'Gracias'),
        'mensaje': _(u'Muchas gracias por haber registrado su estación!!!\n'
                     'En breve nos estaremos comunicando con Ud.'),
    }


def get_registros(request, clave=False):
    '''
    Función para permitir recuperar los datos de los registros de una estación
    meteorológica, mediante una respuesta json.
    '''
    if not clave:
        clave = request.GET.get('clave', False)

    if not clave:
        data = dict(request.GET)
        data['error'] = "No se encontraron datos en {}\n".format(app_title)
        return JsonResponse(data, json_dumps_params={'ensure_ascii': False})

    data = Estaciones.objects.get(clave_lectura=clave).to_dict()
    return JsonResponse(data, json_dumps_params={'ensure_ascii': False})


def put_help(request):
    '''
    Función para permitir recuperar la información de los datos que se permiten
    enviar mediante json para registros nuevos de estaciones meteorológicas,
    mediante una respuesta json.
    '''
    return JsonResponse(post_request_bind, status=200)


def put_registros(request, llave=False):
    '''
    Función para permitir enviar datos de registros nuevos de estaciones
    meteorológicas que se necesitan resguardar, mediante una solicitud json.
    '''
    enviado = {}
    if request.method == 'POST':
        enviado = request.POST
    elif request.method == 'GET':
        enviado = request.GET
    else:
        return JsonResponse(
                    {'mensaje': 'Hubo un error en los datos enviados'},
                    status=405)

    datos = {}
    est = None
    try:
        if not llave:
            llave = enviado.get('clave', '')
        datos['llave'] = llave
        est = Estaciones.objects.get(clave_escritura=llave)
    except Exception:
        datos['errores'] = ['no se ha ingresado una llave válida']
        return JsonResponse(datos, status=404)

    for k, v in post_request_bind.items():
        data = enviado.get(k, False)
        if data:
            datos[k] = data

    try:
        reg = Registros(id_estacion=est)
        reg.cargar_datos_desde_dict(datos)
        reg.save()
        return JsonResponse(reg.to_dict(), status=200)
    except ValidationError as e:
        datos['errores'] = dict(e)
        return JsonResponse(datos, status=400)
    except Exception as e:
        if settings.DEBUG:
            print(traceback.print_exc())
        return JsonResponse({'errores': str(e)}, status=500)


def send_email(body, to_email, subject=None):
    '''
    Función que permite enviar correos electrónicos necesarios para comunicar
    al usuario diversos eventos.
    '''
    admins = [mail_tuple[1] for mail_tuple in settings.MANAGERS]

    if not subject:
        subject = '{} : : Aviso'.format(app_title)

    email = EmailMessage(subject=subject, body=body, to=to_email, bcc=admins)
    email.content_subtype = "html"

    if settings.DEBUG or not settings.EMAIL_HOST_USER:
        print('Simulando envío de correo', '\n',
              email.to,
              strip_tags(body))
    else:
        email.send()
