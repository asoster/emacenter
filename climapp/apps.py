# apps.py
#
# Configuración de la aplicación
#
# LICENSE:  This file is part of EMA Libre.
# EMA Libre is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EMA Libre is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EMA Libre.  If not, see http://www.gnu.org/licenses/.
#
# @copyright  Copyright (c) 2019 GUGLER (http://www.gugler.com.ar)
# @license    https://www.gnu.org/licenses/gpl-3.0.html GPL License
# @link       https://www.gugler.com.ar
# @since      File available since Release v1.0.0


from django.apps import AppConfig

from . import (
    app_name,
    app_title,
    app_animal,
    app_version,
    app_credits,
    app_description,
    app_developers
)


class ClimappConfig(AppConfig):
    name = '{}'.format(app_name)
    verbose_name = '{}'.format(app_title)
    label = '{}'.format(app_title)
    description = [t + '.'
                   for t in app_description.split('.')
                   if t.strip() != '']
    credits = '{}'.format(app_credits)
    animal_name = '{}'.format(app_animal)
    version = '{}'.format(app_version)
    developer = ', '.join(app_developers)
