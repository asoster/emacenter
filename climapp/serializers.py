# serializers.py
#
# Estructuras para serializar modelos
#
# LICENSE:  This file is part of EMA Libre.
# EMA Libre is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EMA Libre is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EMA Libre.  If not, see http://www.gnu.org/licenses/.
#
# @copyright  Copyright (c) 2019 GUGLER (http://www.gugler.com.ar)
# @license    https://www.gnu.org/licenses/gpl-3.0.html GPL License
# @link       https://www.gugler.com.ar
# @since      File available since Release v1.0.0


from rest_framework.serializers import (
    ModelSerializer,
    Serializer,
    SerializerMethodField,
    DateTimeField,
    DecimalField
)

from .models import Estaciones, Registros

from django.contrib.auth.models import User


class UserSerializer(ModelSerializer):
    full_name = SerializerMethodField()

    def get_full_name(self, obj):
        return "{} {}".format(obj.first_name, obj.last_name)

    class Meta:
        model = User
        fields = ('id', 'username', 'full_name', 'email', 'is_active',
                  'is_staff', 'is_superuser')


class UserRetrieveSerializer(ModelSerializer):
    class Meta:
        model = User
        exclude = ('password', 'groups', 'user_permissions')


class RegistrosSerial(ModelSerializer):
    """
    Permite serializar Registros de las Estaciones
    """

    class Meta:
        model = Registros
        exclude = ('id_estacion', 'fecha_alta', 'fecha_baja',
                   'sin_acumulado_lluvia')

    def validate(self, data):
        d = Registros(**dict(data))
        d.clean()
        return data


class EstacionListSerial(ModelSerializer):
    """
    Permite serializar la lista de Estaciones
    """

    class Meta:
        model = Estaciones
        fields = ('id_estacion', 'nombre', 'modelo', 'pais', 'ciudad', 'calle',
                  'latitud', 'longitud', 'altura', 'fecha_ultimo_registro',
                  'estado')


class EstadisticasSerializer(Serializer):
    """
    Permite serializar registros de una Estación, resumidos por hora
    """
    hora = DateTimeField()
    temperatura_externa_max = DecimalField(max_digits=10, decimal_places=2)
    temperatura_externa_min = DecimalField(max_digits=10, decimal_places=2)
    humedad_externa_max = DecimalField(max_digits=10, decimal_places=2)
    humedad_externa_min = DecimalField(max_digits=10, decimal_places=2)
    lluvia_acumulado_hora_max = DecimalField(max_digits=10, decimal_places=2)
    lluvia_acumulado_hora_min = DecimalField(max_digits=10, decimal_places=2)
    luxer_intencidad_max = DecimalField(max_digits=10, decimal_places=2)
    luxer_intencidad_min = DecimalField(max_digits=10, decimal_places=2)
    viento_velocidad_max = DecimalField(max_digits=10, decimal_places=2)
    viento_velocidad_min = DecimalField(max_digits=10, decimal_places=2)
    presion_absoluta_max = DecimalField(max_digits=10, decimal_places=2)
    presion_absoluta_min = DecimalField(max_digits=10, decimal_places=2)
    luxer_uv_max = DecimalField(max_digits=10, decimal_places=2)
    luxer_uv_min = DecimalField(max_digits=10, decimal_places=2)
    presion_relativa_max = DecimalField(max_digits=10, decimal_places=2)
    presion_relativa_min = DecimalField(max_digits=10, decimal_places=2)
    humedad_externa_min = DecimalField(max_digits=10, decimal_places=2)
    humedad_externa_min = DecimalField(max_digits=10, decimal_places=2)
    viento_rafagas_max = DecimalField(max_digits=10, decimal_places=2)


class EstacionSerial(ModelSerializer):
    """
    Permite serializar los datos de una Estación
    """

    registros = RegistrosSerial(many=True, read_only=True,
                                source="ultimos_registros")
    estadisticas = EstadisticasSerializer(many=True, read_only=True,
                                          source="get_estadisticas")

    class Meta:
        model = Estaciones
        fields = ('id_estacion', 'nombre', 'modelo', 'pais', 'ciudad', 'calle',
                  'latitud', 'longitud', 'altura', 'fecha_ultimo_registro',
                  'registros', 'estadisticas')
