# tests.py
#
# Pruebas unitarias de la aplicación
#
# LICENSE:  This file is part of EMA Libre.
# EMA Libre is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EMA Libre is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EMA Libre.  If not, see http://www.gnu.org/licenses/.
#
# @copyright  Copyright (c) 2019 GUGLER (http://www.gugler.com.ar)
# @license    https://www.gnu.org/licenses/gpl-3.0.html GPL License
# @link       https://www.gugler.com.ar
# @since      File available since Release v1.0.0


import datetime
import random

from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import reverse
from django.utils import timezone

from .models import Estaciones, Registros, Calidad, post_request_bind
from .forms import RegisterForm, EstacionesForm, ContactForm
from .admin import (
    AdminRegistros,
    AdminEstaciones,
    AdminCalidad,
    desactivar,
    activar,
    regenerar_clave_lectura,
    regenerar_clave_escritura
)
from .serializers import UserSerializer
from emacenter import settings


class SiteTest(TestCase):
    '''
    Clase para los tests de vistas genéricas
    '''

    def setUp(self):
        self.cliente = Client()
        self.user = User.objects.create_user(
                        'admin', 'admin@gugler.com.ar', 'admin')
        self.prueba = User.objects.create_user(
                        'prueba', 'prueba@gugler.com.ar', 'prueba')

    def test_home_page(self):
        response = self.cliente.get(reverse('home'))
        self.assertEqual(200, response.status_code)

    def test_index_page(self):
        response = self.cliente.get(reverse('index'))
        self.assertEqual(200, response.status_code)

    def test_userinfo_page(self):
        response = self.cliente.get(reverse('user_info'))
        self.assertRedirects(response, reverse('admin:login') +
                             '?next='+reverse('user_info'), status_code=302,
                             target_status_code=200)

        self.cliente.force_login(self.user)
        response = self.cliente.get(reverse('user_info'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['user'].username, self.user.username)
        self.assertTemplateUsed(response, "auth/user_info.html")

    def test_user_update_page(self):
        rev_url = reverse('user_update', args=[self.user.username])
        response = self.cliente.get(rev_url)
        self.assertRedirects(response, reverse('admin:login')+'?next='+rev_url,
                             status_code=302, target_status_code=200)

        self.cliente.force_login(self.user)

        response = self.cliente.get(reverse('user_update',
                                    args=[self.prueba.username]))
        self.assertRedirects(response, rev_url,
                             status_code=302, target_status_code=200)

        response = self.cliente.get(rev_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['user'].username, self.user.username)
        self.assertTemplateUsed(response, "auth/user_form.html")

    def test_login(self):
        self.cliente.login(username=self.user.username,
                           password=self.user.password)
        response = self.cliente.get(reverse('admin:login'))
        self.assertEqual(response.status_code, 200)

    def test_ranking_views(self):
        response = self.cliente.get(reverse('mail_online'))
        self.assertEqual(response.status_code, 200)
        response = self.cliente.get(reverse('mail_online') + '?clave_mail=' +
                                    settings.CLAVE_MAIL_OFFLINE)
        self.assertEqual(response.status_code, 200)
        response = self.cliente.get(reverse('mail_offline') + '?clave_mail=' +
                                    settings.CLAVE_MAIL_OFFLINE)
        self.assertEqual(response.status_code, 200)
        response = self.cliente.get(reverse('mail_ranking') + '?clave_mail=' +
                                    settings.CLAVE_MAIL_OFFLINE)
        self.assertEqual(response.status_code, 200)


class ContactTest(TestCase):
    '''
    Clase para los tests del formulario de ingreso de datos de contacto
    '''

    def setUp(self):
        self.cliente = Client()

    def test_contact_form_valid(self):
        form = ContactForm(data={'email': "user@mp.com",
                                 'name': "user", 'message': "user"})
        self.assertTrue(form.is_valid())

    def test_contact_form_invalid(self):
        form = ContactForm(data={'email': "", 'name': "", 'message': ""})
        self.assertFalse(form.is_valid())

    def test_post_contacto(self):
        response = self.cliente.post(reverse('contact'),
                                     {'name': '', 'email': '', 'message': ''})
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, "contact.html")

    def test_thanks_contacto(self):
        response = self.cliente.get(reverse('contact_thanks'))
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, "message.html")

    def test_post_contacto_fail(self):
        response = self.cliente.post(reverse('contact'))
        self.assertFormError(response, 'form', 'name',
                             'Este campo es requerido.')
        self.assertEqual(200, response.status_code)


nombres = ['uno', 'dos', 'tres', 'cuatro', 'cinco', 'seis', 'siete',
           'ocho', 'nueve', 'cero']
paises = ['Argentina', 'Chile', 'Uruguay', 'Brasil', 'Paraguay',
          'Bolivia', 'Perú']
ciudades = ['Paraná', 'Santa Fe', 'Rosario', 'Córdoba', 'Oro Verde']
modelos = ['Bultos', 'Sonido', 'Propaganda', 'Estallar',
           'Poeta', 'Recolector', 'Furgón', 'Medias', 'Agujero', 'Baldosa']
vientos = ('N', 'NE', 'E', 'SE', 'S', 'SO', 'O', 'NO')


def _get_data_estacion(cantidad=10):
    for c in range(cantidad):
        yield {'nombre': random.choice(nombres),
               'modelo': 'modelo', 'pais': random.choice(paises),
               'ciudad': 'sin datos de la ciudad',
               'calle': 'sin datos de la calle',
               'latitud': random.choice(range(0, 90)),
               'longitud': random.choice(range(0, 180)),
               'altura': random.choice(range(5, 90))}


def _get_data_registro():
    data = {'fecha': random.choice([timezone.now(), datetime.datetime.now()]),
            'viento_direccion_nombre': random.choice(vientos),
            'tanque': random.choice([True, False, None]),
            'software': random.choice(['testing sw', '', '-']),
            'temperatura_externa': random.random() * 60,
            'temperatura_interna': random.random() * 60,
            'humedad_externa': random.random() * 100,
            'humedad_interna': random.random() * 100,
            'punto_de_rocio': random.random() * 100,
            'luxer_intencidad': random.random() * 10,
            'luxer_uv': random.random() * 10,
            'viento_velocidad': random.random() * 100,
            'viento_rafagas': random.random() * 100,
            'viento_direccion': random.random() * 360,
            'presion_absoluta': random.random() * 100,
            'presion_relativa': random.random() * 100,
            'lluvia_actual': random.random() * 100,
            'lluvia_acumulado_hora': random.random() * 100,
            'lluvia_acumulado_diario': random.random() * 100}
    if data['tanque'] is None:
        del data['tanque']
    return data


class SerializerTest(TestCase):
    '''
    Clase para los tests del módulo serializer de Estaciones meteorológicas
    '''

    def setUp(self):
        self.user = User.objects.create_user('admin', 'admin@gugler.com.ar',
                                             'admin')

    def test_user_serializer(self):
        s = UserSerializer()
        for u in User.objects.all():
            self.assertEqual(s.get_full_name(u),
                             '{} {}'.format(u.first_name, u.last_name))


class AdminTest(TestCase):
    '''
    Clase para los tests del módulo admin de Estaciones meteorológicas
    '''

    def setUp(self):
        for e in _get_data_estacion():
            Estaciones.objects.create(**e)

        for e in Estaciones.objects.all():
            data = _get_data_registro()
            del data['fecha']
            Registros.objects.create(id_estacion=e, **data)

    def test_admin_activar(self):
        activar(AdminEstaciones, None, Estaciones.objects.all())
        for e in Estaciones.objects.all():
            self.assertEqual(e.activa, True)

    def test_admin_desactivar(self):
        desactivar(AdminEstaciones, None, Estaciones.objects.all())
        for e in Estaciones.objects.all():
            self.assertEqual(e.activa, False)

    def test_admin_regenerar_lectura(self):
        for e in Estaciones.objects.all():
            clave = e.clave_lectura
            regenerar_clave_lectura(AdminEstaciones, None, [e])
            self.assertNotEqual(e.clave_lectura, clave)

    def test_admin_regenerar_escritura(self):
        for e in Estaciones.objects.all():
            clave = e.clave_escritura
            regenerar_clave_escritura(AdminEstaciones, None, [e])
            self.assertNotEqual(e.clave_escritura, clave)

    def test_admin_registros_add_permission(self):
        e = AdminRegistros(Registros, None)
        self.assertEqual(e.has_add_permission(None), False)

    def test_admin_registros_delete_permission(self):
        e = AdminRegistros(Registros, None)
        self.assertEqual(e.has_delete_permission(None), False)

    def test_admin_get_readonly_fields(self):
        r = AdminRegistros(Registros, None)
        for re in Registros.objects.all():
            self.assertNotEqual(r.get_readonly_fields(None, re), 0)

    def test_admin_calidad_change_permission(self):
        e = AdminCalidad(Calidad, None)
        self.assertEqual(e.has_change_permission(None), False)


class EstacionesTest(TestCase):
    '''
    Clase para los tests de los datos de Estaciones meteorológicas
    '''

    def setUp(self):
        for e in _get_data_estacion():
            Estaciones.objects.create(**e)
        for e in Estaciones.objects.all():
            for m in range(5):
                for f in range(-1, -11, -1):
                    dato = _get_data_registro()
                    dato['fecha'] = timezone.now() + \
                        datetime.timedelta(hours=f * m)
                    Registros.objects.create(id_estacion=e, **dato)
            dato = _get_data_registro()
            del(dato['fecha'])
            Registros.objects.create(id_estacion=e, **dato)

        self.user = User.objects.create_user('admin', 'admin@gugler.com.ar',
                                             'admin')
        self.cliente = Client()

    def test_estacion_form_valid(self):
        form = EstacionesForm(data=_get_data_estacion().__next__())
        self.assertTrue(form.is_valid())

    def test_estacion_form_invalid(self):
        form = EstacionesForm(data={})
        self.assertFalse(form.is_valid())

    def test_register_form_valid(self):
        form = RegisterForm(data={'first_name': "name", 'last_name': "last",
                                  'email': "user@mp.com", 'username': "user",
                                  'password': "password"})
        self.assertTrue(form.is_valid())

    def test_register_form_invalid(self):
        form = RegisterForm(data={'first_name': "", 'last_name': "",
                                  'email': "", 'username': "", 'password': ""})
        self.assertFalse(form.is_valid())

    def test_estaciones_datos(self):
        for e in Estaciones.objects.all():
            self.assertEqual(e.__str__(), '{} ({}, {})'.format(
                            e.nombre, e.pais, e.ciudad))
            self.assertEqual(e.get_absolute_url(),
                             reverse('station_detail', kwargs={'pk': e.pk}))
            self.assertEqual(e.__unicode__(), u'{} ({}, {})'.format(
                             e.nombre, e.pais, e.ciudad))
            self.assertEqual(e.modelo_estacion(), e.modelo)
            self.assertEqual(e.get_nombre_lista(), e.nombre)
            self.assertEqual(e.fecha(), e.fecha_alta)
            self.assertIsNone(e.fecha_baja)
            d = e.to_dict(cantidad_registros=0)
            self.assertEqual(0, len(d['registros']))
            self.assertNotEqual(0, len(d['estadisticas']))

    def test_estaciones_fecha(self):
        for e in Estaciones.objects.all():
            fecha_mod_ant = e.fecha_modificacion
            e.altura += 1
            e.save()
            self.assertNotEqual(e.fecha_modificacion, fecha_mod_ant)
            self.assertNotEqual(e.fecha_modificacion, e.fecha_alta)

    def test_estaciones_activar(self):
        for e in Estaciones.objects.all():
            e.activa = True
            e.desactivar()
            self.assertEqual(e.activa, False)
            e.activar()
            self.assertEqual(e.activa, True)

    def test_estaciones_regenerar_clave(self):
        for e in Estaciones.objects.all():
            clave = e.clave_lectura
            e.regenerar_clave_lectura()
            self.assertNotEqual(e.clave_lectura, clave)
            clave = e.clave_escritura
            e.regenerar_clave_escritura()
            self.assertNotEqual(e.clave_escritura, clave)

    def test_estaciones_post_update(self):
        e = Estaciones.objects.all()[0]
        e.id_usuario = self.user
        e.save()
        self.cliente.force_login(self.user)
        response = self.cliente.post(
            reverse('station_update', kwargs={'pk': e.pk}), {
                'modelo': 'MODELO STATION', 'pais': 'Argentina',
                'ciudad': 'Paraná',
                'calle': 'calle de la estación', 'latitud': -60.25631,
                'longitud': -30.24561, 'altura': 61}
        )
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, "climapp/estaciones_form.html")

    def test_estaciones_filtrar(self):
        e = Estaciones.objects.all()[0]
        self.assertNotEqual(0, len(Estaciones.filtrar_objetos(e.nombre)))
        for e in Estaciones.objects.all():
            for f in Estaciones.filtrar_objetos(e.nombre):
                self.assertEqual(e.nombre, f.nombre)

    def test_estaciones_registros(self):
        for e in Estaciones.objects.all():
            self.assertNotEqual(False, e.ultimo_registro())
            self.assertEqual(10, len(e.ultimos_registros(10)))
            data = _get_data_registro()
            del data['fecha']
            reg = Registros.objects.create(
                    id_estacion=e, **data)
            self.assertEqual(e.ultimo_registro(), reg)
            e.registros.all().delete()
            self.assertEqual(False, e.ultimo_registro())

    def test_estaciones_en_linea(self):
        for e in Estaciones.objects.all():
            self.assertEqual(e.estado(), u'En línea')
            e.fecha_ultimo_registro = timezone.now() - \
                datetime.timedelta(minutes=59)
            self.assertEqual(e.estado(), u'Fuera de línea')
            e.fecha_ultimo_registro = None
            self.assertEqual(e.estado(), u'Fuera de línea')


class RegistrosTest(TestCase):
    '''
    Clase para los tests de los datos de Registros de estaciones meteorológicas
    '''

    def setUp(self):
        for e in _get_data_estacion():
            est = Estaciones.objects.create(**e)
            for m in range(5):
                for f in range(-1, -11, -1):
                    dato = _get_data_registro()
                    dato['fecha'] = timezone.now() + \
                        datetime.timedelta(hours=f * m)
                    Registros.objects.create(id_estacion=est, **dato)
        self.cliente = Client()

    def test_registros_datos(self):
        for e in Estaciones.objects.all():
            for r in Registros.objects.all().filter(id_estacion=e):
                self.assertEqual(r.__str__(), '{} {}'.format(
                                e.modelo, r.fecha))
                self.assertEqual(r.__unicode__(), u'{} {}'.format(
                                e.modelo, r.fecha))
                self.assertEqual(r.modelo_estacion(), '{}'.format(e.modelo))
                self.assertEqual(r.get_nombre_lista(), '{}'.format(e))

    def test_registro_nombre_viento(self):
        reg = Registros(id_estacion=Estaciones.objects.all()[0])
        vientos = {
            0: 'Norte', 22: 'NorNorEste', 45: 'NorEste',
            70: 'EsteNorEste', 90: 'Este', 130: 'EsteSurEste', 135: 'SurEste',
            145: 'SurSurEste', 180: 'Sur', 200: 'SurSurOeste',
            225: 'SurOeste', 250: 'OesteSurOeste', 270: 'Oeste',
            300: 'OesteNorOeste', 315: 'NorOeste', 340: 'NorNorOeste',
            367: 'N/A'}
        for c, v in vientos.items():
            reg.viento_direccion = c
            self.assertEqual(reg.nombre_viento(), v)

    def test_cargar_registros_desde_dict(self):
        uno = Estaciones.objects.all()[0]
        reg = Registros(id_estacion=uno)

        datos = {}
        for _ in range(7):
            for k, v in post_request_bind.items():
                if v == 'decimal':
                    datos[k] = round(random.uniform(2.5, 10.0), 2)
                elif v == 'string' and k != 'viento_direccion_nombre':
                    datos[k] = 'texto aleatorio'
                elif v == 'timestamp':
                    datos[k] = timezone.now()
            datos['campo_erroneo'] = 'algun valor'
            reg.cargar_datos_desde_dict(datos)
            datos['fecha'] = random.choice([None, datos['fecha']])
            if datos['fecha'] is None:
                reg.fecha = None
                del datos['fecha']
            reg.clean()
            for k, v in post_request_bind.items():
                if k in datos:
                    self.assertEqual(getattr(reg, k), datos[k])

    def test_post_registro(self):
        for e in Estaciones.objects.all():
            response = self.cliente.post(
                reverse('post_llave', kwargs={'llave': e.clave_escritura}),
                _get_data_registro())
            self.assertEqual(200, response.status_code)
            response = self.cliente.post(
                reverse('post_llave', kwargs={'llave': e.clave_escritura}), {
                    'humedad_externa': 0, 'humedad_interna': 0,
                    'viento_direccion': 0,
                    'temperatura_externa': 0, 'temperatura_interna': 0,
                    'luxer_uv': 0, 'softwaretype': 'pruebas'
                })
            self.assertEqual(400, response.status_code)
            self.assertEqual(6, len(response.json()['errores']))

    def test_post_registro_acumulado_lluvia(self):
        uno = Estaciones.objects.all()[0]
        uno.fecha_ultimo_registro = None

        Registros.objects.filter(id_estacion=uno).delete()

        acum_hora, uno.lluvia_acumulado_hora = 0, 0
        acum_dia, uno.lluvia_acumulado_diario = 0, 0
        acum_semana, uno.lluvia_acumulado_semanal = 0, 0
        acum_mes, uno.lluvia_acumulado_mensual = 0, 0
        acum_anio, uno.lluvia_acumulado_anual = 0, 0
        acum_total, uno.lluvia_acumulado_total = 0, 0

        uno.save()

        fecha_envio = timezone.now()
        fecha_anterior = None

        for nro in range(1000):
            lluvia_aleatoria = round(random.uniform(2.5, 10.0), 2)
            lluvia_hora = round(lluvia_aleatoria/2, 2)

            fecha_envio = (fecha_envio + datetime.timedelta(
                    minutes=random.randint(30, 359))).astimezone()

            if not fecha_anterior:
                fecha_anterior = fecha_envio

            if fecha_anterior.astimezone().isocalendar() != \
                    fecha_envio.astimezone().isocalendar():
                if fecha_anterior.astimezone().year != \
                        fecha_envio.astimezone().year:
                    acum_anio = 0
                if fecha_anterior.astimezone().month != \
                        fecha_envio.astimezone().month:
                    acum_mes = 0
                if fecha_anterior.astimezone().isocalendar()[1] != \
                        fecha_envio.astimezone().isocalendar()[1]:
                    acum_semana = 0
                acum_total += acum_dia
                acum_anio += acum_dia
                acum_mes += acum_dia
                acum_semana += acum_dia
                acum_dia = 0
                acum_hora = 0

            if fecha_anterior.astimezone().hour != \
                    fecha_envio.astimezone().hour:
                acum_hora = 0

            response = self.cliente.post(
                reverse('post_llave', kwargs={'llave': uno.clave_escritura}), {
                    'temperatura_externa': 15,
                    'fecha': fecha_envio,
                    'humedad_externa': 50, 'humedad_interna': 16,
                    'punto_de_rocio': 23, 'luxer_intencidad': 0, 'luxer_uv': 3,
                    'lluvia_acumulado_diario': lluvia_aleatoria,
                    'lluvia_acumulado_hora': lluvia_hora})

            if lluvia_aleatoria > acum_dia:
                acum_dia = lluvia_aleatoria

            if lluvia_hora > acum_hora:
                acum_hora = lluvia_hora

            if settings.DEBUG:
                self.assertEqual(200, response.status_code)
                self.assertEqual(
                    round(acum_dia, 2),
                    float(response.json()['lluvia_acumulado_diario']))
                self.assertEqual(
                    round(acum_hora, 2),
                    float(response.json()['lluvia_acumulado_hora']))
                self.assertEqual(
                    round(acum_semana, 2),
                    float(response.json()['lluvia_acumulado_semanal']))
                self.assertEqual(
                    round(acum_mes, 2),
                    float(response.json()['lluvia_acumulado_mensual']))
                self.assertEqual(
                    round(acum_anio, 2),
                    float(response.json()['lluvia_acumulado_anual']))
                self.assertEqual(
                    round(acum_total, 2),
                    float(response.json()['lluvia_acumulado_total']))

            fecha_anterior = fecha_envio

    def test_post_registro_validation_fail(self):
        e = Estaciones.objects.all()[0]
        response = self.cliente.post(
            reverse('post_llave', kwargs={'llave': e.clave_escritura}),
            {'temperatura_externa': 115, 'luxer_intencidad': 0, 'luxer_uv': 13,
             'humedad_externa': 50, 'humedad_interna': 16,
             'punto_de_rocio': 23})
        self.assertEqual(400, response.status_code)
        self.assertTrue(
            'luxer_uv' in response.json()['errores'].keys() and
            'temperatura_externa' in response.json()['errores'].keys())

    def test_post_registro_write_key_fail(self):
        response = self.cliente.post('/post/{}/'.format('clave-inexistente'), {
            'temperatura_externa': 115,
            'humedad_externa': 50, 'humedad_interna': 16, 'punto_de_rocio': 23,
            'luxer_intencidad': 0, 'luxer_uv': 13
        })
        self.assertEqual(404, response.status_code)

    def test_registro_validacion(self):
        for e in Estaciones.objects.all():
            # acum_total = float(e.lluvia_acumulado_total)
            for m in range(5):
                for f in range(-1, -11, -1):
                    dato = _get_data_registro()
                    dato['fecha'] = timezone.now() + \
                        datetime.timedelta(hours=f * m)
                    reg = Registros.objects.create(id_estacion=e, **dato)
                    self.assertEqual(dato['fecha'], reg.fecha)
            dato = _get_data_registro()
            del(dato['fecha'])
            reg = Registros.objects.create(id_estacion=e, **dato)
            self.assertIsNotNone(reg.fecha)


class ApiViewSetTest(TestCase):
    endpoints = ('station', 'estacion')
    endpoints_login = ('user', 'data', 'record', 'registro')

    def test_endpoints(self):
        self.user = User.objects.create_user(username='root', password='root',
                                             email='root@localhost')
        self.user.is_staff = True
        self.user.is_superuser = True
        self.user.save()
        for url in self.endpoints:
            response = self.client.get('/api/{}/'.format(url))
            self.assertEqual(response.status_code, 200)

        self.client.force_login(self.user)
        for url in self.endpoints_login:
            response = self.client.get('/api/{}/'.format(url))
            self.assertEqual(response.status_code, 200)
