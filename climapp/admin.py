# admin.py
#
# Registro de los modelos en el admin de Django
#
# LICENSE:  This file is part of EMA Libre.
# EMA Libre is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EMA Libre is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EMA Libre.  If not, see http://www.gnu.org/licenses/.
#
# @copyright  Copyright (c) 2019 GUGLER (http://www.gugler.com.ar)
# @license    https://www.gnu.org/licenses/gpl-3.0.html GPL License
# @link       https://www.gugler.com.ar
# @since      File available since Release v1.0.0


from django.contrib import admin

from .models import Estaciones, Registros, Calidad

from . import app_title, app_animal

admin.site.site_header = '{} - {}'.format(app_title, app_animal)
admin.site.site_title = '{}'.format(app_title)


def activar(modeladmin, request, queryset):
    for obj in queryset:
        obj.activar()


activar.short_description = "Activar"


def desactivar(modeladmin, request, queryset):
    for obj in queryset:
        obj.desactivar()


desactivar.short_description = "Desactivar"


def regenerar_clave_lectura(modeladmin, request, queryset):
    for obj in queryset:
        obj.regenerar_clave_lectura()


regenerar_clave_lectura.short_description = "Regenerar clave de lectura"


def regenerar_clave_escritura(modeladmin, request, queryset):
    for obj in queryset:
        obj.regenerar_clave_escritura()


regenerar_clave_escritura.short_description = "Regenerar clave de escritura"


@admin.register(Estaciones)
class AdminEstaciones(admin.ModelAdmin):
    fieldsets = (
        ('General', {
            'fields': (
                        'id_usuario', 'nombre', 'fecha_ultimo_registro',
                        'modelo', 'pais', 'ciudad', 'calle', 'latitud',
                        'longitud', 'altura')}),
        ('Claves', {
                'classes': ('wide', 'collapse', 'extrapretty'),
                'fields': ('clave_lectura', 'clave_escritura')}),
        ('Acumulado de lluvias', {
                'classes': ('wide', 'collapse', 'extrapretty'),
                'fields': (
                            'lluvia_acumulado_hora',
                            'lluvia_acumulado_diario',
                            'lluvia_acumulado_semanal',
                            'lluvia_acumulado_mensual',
                            'lluvia_acumulado_anual',
                            'lluvia_acumulado_total')}),
    )

    readonly_fields = ('id_usuario', 'fecha_ultimo_registro', 'clave_lectura',
                       'clave_escritura', 'lluvia_acumulado_hora',
                       'lluvia_acumulado_diario', 'lluvia_acumulado_semanal',
                       'lluvia_acumulado_mensual', 'lluvia_acumulado_anual',
                       'lluvia_acumulado_total')
    list_display = ('__str__', 'pais', 'ciudad', 'calle', 'latitud',
                    'longitud', 'activa', 'clave_lectura')
    search_fields = ('nombre', 'modelo', 'pais', 'ciudad')
    list_filter = ('id_usuario', 'activa', 'modelo', 'pais', 'ciudad')
    ordering = ('pais', 'ciudad', 'nombre')
    list_per_page = 25
    actions = [
                activar, desactivar,
                regenerar_clave_lectura, regenerar_clave_escritura
            ]


@admin.register(Registros)
class AdminRegistros(admin.ModelAdmin):
    readonly_fields = ['id_estacion', ]
    fieldsets = (
      ('General', {
            'fields': ('fecha_ingreso', 'fecha', 'id_estacion',
                       'software', 'tanque')}),
      ('Datos internos', {
            'classes': ('wide', 'collapse', 'extrapretty'),
            'fields': ('temperatura_interna', 'humedad_interna')}),
      ('Datos externos', {
            'classes': ('wide', 'collapse', 'extrapretty'),
            'fields': ('temperatura_externa',
                       'humedad_externa', 'punto_de_rocio', 'luxer_intencidad',
                       'luxer_uv', 'presion_absoluta', 'presion_relativa')}),
      ('Viento', {
            'classes': ('wide', 'collapse', 'extrapretty'),
            'fields': ('viento_velocidad', 'viento_rafagas',
                       'viento_direccion', 'viento_direccion_nombre')}),
      ('Lluvia', {
            'classes': ('wide', 'collapse', 'extrapretty'),
            'fields': ('lluvia_actual', 'lluvia_acumulado_hora',
                       'lluvia_acumulado_diario', 'sin_acumulado_lluvia',
                       'lluvia_acumulado_semanal', 'lluvia_acumulado_mensual',
                       'lluvia_acumulado_anual', 'lluvia_acumulado_total')}),
    )
    list_display = ('fecha', 'get_nombre_lista', 'temperatura_externa',
                    'humedad_externa', 'lluvia_acumulado_hora',
                    'presion_absoluta', 'luxer_intencidad')
    search_fields = ('id_estacion__modelo', 'id_estacion__pais',
                     'id_estacion__ciudad', 'software',
                     'id_estacion__id_usuario__username')
    list_filter = ('fecha', 'software', 'id_estacion__pais',
                   'id_estacion__ciudad',
                   'id_estacion', 'id_estacion__id_usuario')
    ordering = ('-fecha', 'id_estacion')
    list_per_page = 25

    def get_readonly_fields(self, request, obj=None):
        return list(set(
                    list(self.readonly_fields) +
                    [field.name for field in obj._meta.fields] +
                    [field.name for field in obj._meta.many_to_many]))

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Calidad)
class AdminCalidad(admin.ModelAdmin):
    fieldsets = (
        ('General', {
            'fields': (
                        'registro', 'tanque', 'observacion')}),
        ('Calidad', {
                'fields': ('nivel0', 'nivel1', 'nivel2', 'nivel3', 'nivel4',
                           'nivel5', 'nivel6')})
    )

    def has_change_permission(self, request, obj=None):
        return False

    list_display = ('registro', 'tanque', 'nivel0', 'nivel1', 'nivel2',
                    'nivel3', 'nivel4', 'nivel5', 'nivel6')
    search_fields = ('registro__id_estacion__nombre',)
    list_filter = ('registro__id_estacion', 'tanque', 'nivel0', 'nivel1',
                   'nivel2', 'nivel3', 'nivel4', 'nivel5', 'nivel6')
    list_per_page = 25
