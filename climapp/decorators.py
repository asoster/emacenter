# decorators.py
#
# Decoradores para reutilizar en la aplicación
#
# LICENSE:  This file is part of EMA Libre.
# EMA Libre is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EMA Libre is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EMA Libre.  If not, see http://www.gnu.org/licenses/.
#
# @copyright  Copyright (c) 2019 GUGLER (http://www.gugler.com.ar)
# @license    https://www.gnu.org/licenses/gpl-3.0.html GPL License
# @link       https://www.gugler.com.ar
# @since      File available since Release v1.0.0


from django.contrib.auth.decorators import login_required

from .apps import ClimappConfig


class LoginRequired(object):
    @classmethod
    def as_view(self, **kwargs):
        view = super(LoginRequired, self).as_view(**kwargs)
        return login_required(view)


class ContextoVista(object):
    contexto = {}

    def get_context_data(self, **kwargs):
        context = super(ContextoVista, self).get_context_data(**kwargs)
        context.update({
                'titulo': ClimappConfig.verbose_name,
                'verbose_name': ClimappConfig.verbose_name,
                'app': ClimappConfig.name,
                'label': ClimappConfig.label,
                'description': ClimappConfig.description,
                'credits': ClimappConfig.credits,
                'version': ClimappConfig.version,
                'animal_name': ClimappConfig.animal_name,
            })
        context.update(self.contexto)
        return context
