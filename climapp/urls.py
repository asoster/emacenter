# urls.py
#
# Se definen las url que va a servir la aplicación
#
# LICENSE:  This file is part of EMA Libre.
# EMA Libre is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EMA Libre is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EMA Libre.  If not, see http://www.gnu.org/licenses/.
#
# @copyright  Copyright (c) 2019 GUGLER (http://www.gugler.com.ar)
# @license    https://www.gnu.org/licenses/gpl-3.0.html GPL License
# @link       https://www.gugler.com.ar
# @since      File available since Release v1.0.0


from django.conf.urls import url, include
from django.urls import path
from django.templatetags.static import static
from django.views.generic import RedirectView

from . import views, viewsets, app_title
from .routers import router

from rest_framework.documentation import include_docs_urls


urlpatterns = [
    url(r'^$', views.HomePageView.as_view(), name='home'),
    url(r'^index/$', views.HomePageView.as_view(), name='index'),

    url(r'^online/$', views.MailOfflineView.as_view(), {'tipo': 'online'},
        name='mail_online'),
    url(r'^offline/$', views.MailOfflineView.as_view(), {'tipo': 'offline'},
        name='mail_offline'),
    url(r'^ranking/$', views.MailOfflineView.as_view(), {'tipo': 'ranking'},
        name='mail_ranking'),

    url(r'^accounts/info/$', views.UserInfoPageView.as_view(),
        name='user_info'),
    url(r'^accounts/update/(?P<slug>[\-\w]+)/$', views.UpdateProfile.as_view(),
        name='user_update'),

    # Formulario para registrar una nueva estación
    url(r'^station/new/$', views.EstacionCreate.as_view(),
        name='registro'),
    url(r'^station/thanks/$', views.StationGraciasView.as_view(),
        name='station_thanks'),

    # Se permite acceso a los datos de estaciones
    url(r'^station/list/$', views.StationListView.as_view(),
        name='station_list'),
    url(r'^station/(?P<pk>[0-9]+)/$', views.StationDetailView.as_view(),
        name='station_detail'),
    url(r'^station/(?P<pk>[0-9]+)/edit/$', views.EstacionUpdate.as_view(),
        name='station_update'),

    # Se recibe información por GET o POST
    url(r'^post/(?P<llave>[-\w]+)/$', views.put_registros, name='post_llave'),
    url(r'^post/$', views.put_registros, name='post'),
    url(r'^post/help$', views.put_help, name='post_help'),

    # Formulario de contacto
    url(r'^contact/$', views.ContactView.as_view(), name='contact'),
    url(r'^contact/thanks/$', views.GraciasView.as_view(),
        name='contact_thanks'),

    # Se imprime información por JSON
    url(r'^api/get_registros/(?P<clave>[-\w]+)/$', views.get_registros,
        name='get_llave'),
    url(r'^api/get/(?P<clave_lectura>[-\w]+)/$',
        viewsets.EstacionVista.as_view({'get': 'retrieve'}),
        name='get_con_llave'),

    path('favicon.ico',
         RedirectView.as_view(url=static('img/favicon.png'))),

    url(r'^api/', include(router.urls)),
    url(r'^api/docs/', include_docs_urls(
        title='API Rest {}'.format(app_title),
        description='Referencia documental del API Rest {}'.format(app_title),
        public=False)),
    path('api-auth/', include('rest_framework.urls')),
]
