# populate_db.py
#
# Armado de datos para desarrollo y pruebas de la aplicación
#
# LICENSE:  This file is part of EMA Libre.
# EMA Libre is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EMA Libre is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EMA Libre.  If not, see http://www.gnu.org/licenses/.
#
# @copyright  Copyright (c) 2019 GUGLER (http://www.gugler.com.ar)
# @license    https://www.gnu.org/licenses/gpl-3.0.html GPL License
# @link       https://www.gugler.com.ar
# @since      File available since Release v1.0.0


from django.core.management.base import BaseCommand

from django.contrib.auth.models import User

from climapp.models import Estaciones, Registros

import string
import random
import datetime
from django.utils import timezone


class Command(BaseCommand):
    """
    Comandos para generar una base de datos de desarrollo
    """

    args = '<ninguno>'
    help = '''
    Este módulo permite crear estaciones automáticamente dentro de la DB.
    Es recomendable utilizarlo sólo en entornos de desarrollo.
    '''

    def _create_estaciones(self):

        usuarios = ['exequiel', 'alexis', 'martin', 'fede', 'jose', 'diego',
                    'maria', 'damian', 'emanuel', 'hernan']

        User.objects.all().exclude(is_superuser=True).delete()
        Estaciones.objects.all().delete()

        print('Creando usuarios genéricos para pruebas ... ')

        for usuario in usuarios:
            user, created = User.objects.get_or_create(username=usuario)
            if created:
                user.set_password(usuario)
                user.save()
                print('Estación para {}'.format(user.username))
                self._nueva_estacion(user)

    def _nueva_estacion(self, usuario):

        paises = ['Argentina']
        ciudades = ['Paraná', 'Santa Fe', 'Rosario', 'Córdoba', 'Oro Verde']
        nombre_estacion = [
            'Bultos', 'Sonido', 'Propaganda', 'Estallar',
            'Poeta', 'Recolector', 'Furgón', 'Medias', 'Agujero', 'Baldosa']

        modelo = ''.join(random.choice(
                            string.ascii_uppercase + string.digits)
                         for _ in range(15))
        calle = ''.join(random.choice(string.ascii_uppercase)
                        for _ in range(10)) + ' ' + \
                ''.join(random.choice(string.digits)
                        for _ in range(4))

        return Estaciones.objects.create(
                    nombre=' '.join(random.sample(nombre_estacion, 3)),
                    modelo=modelo,
                    pais=random.choice(paises),
                    ciudad=random.choice(ciudades),
                    id_usuario=usuario, calle=calle)

    def _nuevo_registro(self, estacion, fecha):
        vientos = ('N', 'NE', 'E', 'SE', 'S', 'SO', 'O', 'NO')
        return Registros.objects.create(
                    id_estacion=estacion,
                    fecha=fecha,
                    # ~ tanque=random.choice([True, False, None]),
                    viento_direccion_nombre=random.choice(vientos),
                    software='populated sw',
                    temperatura_externa=random.random() * 60,
                    temperatura_interna=random.random() * 60,
                    humedad_externa=random.random() * 100,
                    humedad_interna=random.random() * 100,
                    punto_de_rocio=random.random() * 100,
                    luxer_intencidad=random.random() * 10,
                    luxer_uv=random.random() * 10,
                    viento_velocidad=random.random() * 100,
                    viento_rafagas=random.random() * 100,
                    viento_direccion=random.random() * 360,
                    presion_absoluta=random.random() * 100,
                    presion_relativa=random.random() * 100,
                    lluvia_actual=random.random() * 100,
                    lluvia_acumulado_hora=random.random() * 100,
                    lluvia_acumulado_diario=random.random() * 100)

    def _create_registros(self):
        print('Creando registros genéricos para pruebas ... ')
        for estacion in Estaciones.objects.all():
            print(estacion, end=': ')
            cantidad = 0
            for m in range(random.randint(15, 45)):
                for f in range(-1, -11, -1):
                    self._nuevo_registro(
                        estacion, timezone.now() +
                        datetime.timedelta(hours=f * m)).save()
                    cantidad += 1
            print('{} registros creados [OK]'.format(cantidad))

    def _mover_registros_ayer(self):
        print('Moviendo registros para pruebas ... ')
        for e in Estaciones.objects.all()[:5]:
            print(e, end=': ')
            cantidad = random.randint(20, 220)
            for r in Registros.objects.filter(id_estacion=e.pk) \
                    .filter(fecha__lt=timezone.now().replace(
                            hour=0, minute=0, second=0, microsecond=0)) \
                    .order_by('-fecha')[:cantidad]:
                r.fecha -= datetime.timedelta(days=1)
                r.fecha_ingreso = r.fecha
                r.save()
            print('{} registros movidos [OK]'.format(cantidad))

    def handle(self, *args, **options):

        if Estaciones.objects.all().count() == 0:
            self._create_estaciones()
        self._create_registros()
        self._mover_registros_ayer()

        for e in Estaciones.objects.all()[4:]:
            e.fecha_ultimo_registro = timezone.now() - \
                timezone.timedelta(minutes=random.randint(10, 150))
            e.save()
