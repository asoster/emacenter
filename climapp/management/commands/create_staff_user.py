# climapp/management/commands/create_staff_user

import logging

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

logger = logging.getLogger('django')


class Command(BaseCommand):
    help = "Command to create users."

    def add_arguments(self, parser):
        parser.add_argument('-s', '--super', action='store_true',
                            help='Is super user?')
        parser.add_argument('-p', '--password', type=str, help='User password')
        parser.add_argument('-u', '--user', type=str, help='User name')

    def handle(self, *args, **kwargs):

        is_super = kwargs['super']
        password = kwargs['password']
        user = kwargs['user']

        logger.info('Iniciando la creación del usuario.')
        try:
            u = User.objects.create_superuser(user, user + '@localhost',
                                              password)
            u.is_staff = True
            u.is_superuser = is_super
            u.save()
        except Exception:
            logger.error('Hubo un error en la creación del usuario')

        logger.info('Fin..')
