# __init__.py
#
# Configuración básica de la aplicación
#
# LICENSE:  This file is part of EMA Libre.
# EMA Libre is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EMA Libre is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EMA Libre.  If not, see http://www.gnu.org/licenses/.
#
# @copyright  Copyright (c) 2019 GUGLER (http://www.gugler.com.ar)
# @license    https://www.gnu.org/licenses/gpl-3.0.html GPL License
# @link       https://www.gugler.com.ar
# @since      File available since Release v1.0.0


app_name = 'climapp'
app_title = 'EMA Libre Center'
app_animal = 'Capibara'
app_resumen = 'Aplicación para la administración de estaciones meteorológicas'
app_description = '''
Ema Libre Center es Software Libre y Gratuito, y el módulo principal del
proyecto EMA Libre, tiene como objetivo validar, catalogar y almacenar en una
base de datos información meteorológica recibida por cualquier software que
implemente su API REST pública, permitiendo de esta forma crear una Red Abierta
de EMA Libre (Red Abierta de Estaciones Meteorológicas Automáticas Libres).
Es importante destacar que Ema Center almacena datos que son enviados
principalmente por nuestro software oficial cliente “Ema Libre Carpincho”, pero
nada impide que una aplicación de un tercero utilizar esta API REST ya que la
misma es pública y libre.
Este software se encuentra desarrollado y mantenido por el Laboratorio de
Investigación Gugler perteneciente a la Facultad de Ciencia y Tecnología de la
Universidad Autónoma de Entre Ríos, toda esta tarea se realiza utilizando
únicamente software libre y publicado bajo la licencia GPL v3.
'''
app_credits = '@gugler'
app_version = 'v1.1.0'
app_developers = ('alexis@gugler.com.ar')
