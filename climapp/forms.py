# forms.py
#
# Formularios que se utilizan en la aplicación
#
# LICENSE:  This file is part of EMA Libre.
# EMA Libre is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EMA Libre is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EMA Libre.  If not, see http://www.gnu.org/licenses/.
#
# @copyright  Copyright (c) 2019 GUGLER (http://www.gugler.com.ar)
# @license    https://www.gnu.org/licenses/gpl-3.0.html GPL License
# @link       https://www.gugler.com.ar
# @since      File available since Release v1.0.0


from django import forms

from django.contrib.auth.models import User

from .models import Estaciones, Registros

from django.utils.translation import ugettext_lazy as _


class BaseForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(BaseForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'


class ContactForm(BaseForm):
    name = forms.CharField(required=True, label=_('Su nombre'))
    email = forms.EmailField(required=True, label=_('Correo electrónico'))
    message = forms.CharField(required=True, widget=forms.Textarea,
                              label=_('Su mensaje'))


class UserForm(forms.ModelForm, BaseForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']


class RegisterForm(forms.ModelForm, BaseForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'username', 'password']


class EstacionesForm(forms.ModelForm, BaseForm):
    class Meta:
        model = Estaciones
        fields = ['nombre', 'modelo', 'pais', 'ciudad', 'calle', 'latitud',
                  'longitud', 'altura']


class RegistrosForm(forms.ModelForm, BaseForm):
    class Meta:
        model = Registros
        fields = ['humedad_externa', 'humedad_interna', 'luxer_uv',
                  'temperatura_externa', 'temperatura_interna',
                  'viento_direccion']
        exclude = ['fecha', 'viento_direccion_nombre', 'fecha_ingreso',
                   'fecha_alta', 'fecha_baja', 'lluvia_acumulado_semanal',
                   'lluvia_acumulado_mensual', 'lluvia_acumulado_anual',
                   'lluvia_acumulado_total']
