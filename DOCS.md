
# Documentación :: EMA Center

## Estructura de la base de datos

EMA Center utiliza dos tablas principales para el resguardo de los datos:
* Estaciones
* Registros
* Calidad

Aquí se detallan los campos de cada una de ellas:

| Tabla | Campo | Tipo |
|-------|-------|------|
| Estaciones | id_estacion | PrimaryKey |
| Estaciones | fecha_alta | DateTime |
| Estaciones | fecha_modificacion | DateTime |
| Estaciones | activa | Boolean |
| Estaciones | clave_lectura | Char |
| Estaciones | clave_escritura | Char |
| Estaciones | modelo | Char |
| Estaciones | pais | Char |
| Estaciones | ciudad | Char |
| Estaciones | calle | Char |
| Estaciones | latitud | Decimal |
| Estaciones | longitud | Decimal |
| Estaciones | altura | Decimal |
| Estaciones | fecha_ultimo_registro | DateTime |
| Estaciones | lluvia_acumulado_hora | Decimal |
| Estaciones | lluvia_acumulado_diario | Decimal |
| Estaciones | lluvia_acumulado_semanal | Decimal |
| Estaciones | lluvia_acumulado_mensual | Decimal |
| Estaciones | lluvia_acumulado_anual | Decimal |
| Estaciones | lluvia_acumulado_total | Decimal |
| Estaciones | id_usuario | ForeignKey |
| Estaciones | nombre | Char |
| Estaciones | fecha_baja | DateTime |
| Registros | id_registro | PrimaryKey |
| Registros | fecha | DateTime |
| Registros | fecha_alta | DateTime |
| Registros | fecha_baja | DateTime |
| Registros | software | Char |
| Registros | viento_direccion_nombre | Char |
| Registros | temperatura_externa | Decimal |
| Registros | temperatura_interna | Decimal |
| Registros | humedad_externa | Decimal |
| Registros | humedad_interna | Decimal |
| Registros | punto_de_rocio | Decimal |
| Registros | luxer_intencidad | Decimal |
| Registros | luxer_uv | Decimal |
| Registros | viento_velocidad | Decimal |
| Registros | viento_rafagas | Decimal |
| Registros | viento_direccion | Decimal |
| Registros | presion_absoluta | Decimal |
| Registros | presion_relativa | Decimal |
| Registros | lluvia_actual | Decimal |
| Registros | lluvia_acumulado_hora | Decimal |
| Registros | lluvia_acumulado_diario | Decimal |
| Registros | lluvia_acumulado_semanal | Decimal |
| Registros | lluvia_acumulado_mensual | Decimal |
| Registros | lluvia_acumulado_anual | Decimal |
| Registros | lluvia_acumulado_total | Decimal |
| Registros | id_estacion | ForeignKey |
| Registros | sin_acumulado_lluvia | Boolean |
| Registros | fecha_ingreso | DateTime |
| Calidad | id | PrimaryKey |
| Calidad | registro | ForeignKey(Registros) |
| Calidad | observacion | Char |
| Calidad | tanque | Boolean |
| Calidad | nivel0 | Boolean |
| Calidad | nivel1 | Boolean |
| Calidad | nivel2 | Boolean |
| Calidad | nivel3 | Boolean |
| Calidad | nivel4 | Boolean |
| Calidad | nivel5 | Boolean |
| Calidad | nivel6 | Boolean |


## Registro de nuevas estaciones meteorológicas

Dentro de EMACenter se encuentra disponible un formulario de registro de
estaciones meteorológicas, tendrá que completar los datos y enviarlos para
terminar el proceso, posteriormente se le enviará un correo electrónico de
bienvenida al sistema. Este proceso de alta creará un usuario para administrar
la estación meteorológica y las claves de lectura y escritura para la misma.

## Envío de datos meteorológicos

Para poder enviar registros de datos meteorológicos se facilita una clave de
escritura, que puede ser modificada cuando se requiera, esta clave es
alfanumérica con la siguiente estructura:
```
AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAAAAAA
```
Donde A puede ser un caracter de letras minúsculas o un dígito de 0 a 9

Está clave es generada automáticamente por el sistema cuando se registra la
estación y puede ser modificada cuando se requiera.

La url que deberá utilizar para el envío de datos de cada estación será:
```
emacenter.gugler.com.ar/post/AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAAAAAA/
```
Si el envío de datos es correcto, el servidor responderá con un código 200.

Para el envío de datos, se deberá hacer una petición vía POST a EMA Center con
los siguientes datos:

| Campo | Tipo | Obligatorio | Unidad |
|-------|------|-------------|--------|
| fecha | Timestamp | No | UTC |
| temperatura_externa | Decimal | Si | º C |
| temperatura_interna | Decimal | Si | º C |
| humedad_externa | Decimal | Si | % |
| humedad_interna | Decimal | Si | % |
| punto_de_rocio | Decimal | No | º C |
| luxer_intencidad | Decimal | No | Klux |
| luxer_uv | Decimal | Si | Índice UV|
| viento_velocidad | Decimal | No | Km/h |
| viento_rafagas | Decimal | No | Km/h |
| viento_direccion | Decimal | Si | grados |
| presion_absoluta | Decimal | No | Hpa |
| presion_relativa | Decimal | No | Hpa |
| lloviendo | Decimal | No | mm |
| lluvia_actual | Decimal | No | mm |
| lluvia_acumulado_hora | Decimal | No | mm |
| lluvia_acumulado_diario | Decimal | No | mm |
| software | Texto | No | - |

### Validaciones del registro

Una vez recibido el registro enviado por método POST, EMA Center realiza
diversas validaciones antes de persistir los datos.

| Campo | Tipo | Mínimo | Máximo |
|-------|------|--------|--------|
| fecha | Timestamp | - | Ahora |
| temperatura_externa | Decimal | 0 | 60 |
| temperatura_interna | Decimal | 0 | 60 |
| humedad_externa | Decimal | 0 | 10 |
| humedad_interna | Decimal | 0 | 100 |
| luxer_uv | Decimal | 0 | 100 |
| viento_direccion | Decimal | 0 | 360 |

## Lectura de datos meteorológicos

EMA Center provee una API para obtener los datos de las estaciones
meteorológicas.
Esta información devuelta por el servidor tendrá un formato JSON para facilitar
su procesamiento en el software cliente.

A partir de la clave de lectura de la estación se pueden obtener los datos de la
estación y los últimos registros enviados:

```
emacenter.gugler.com.ar/api/get/AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAAAAAA/
```

## Lectura de datos por medio de API Rest

A partir de ahora, se entiende que todas las url estan precedidas por la URL del
servidor donde esta instalado EMA Center.

Otra manera de acceder es directamente con el API. Se provee las distintas url
para acceder a los distintos modelos de datos, siempre mediante peticiones GET.

| URL | Auth | Descripción |
|-----|------|-------------|
| /api/user | Si | Listado de usuarios |
| /api/user/\<id\> | Si | Información detallada del usuario. Necesita el identificador del usuario |
| /api/station | No | Listado de estaciones |
| /api/station/\<id\> | No | Información detallada de la estación. Necesita el identificador de la estación |
| /api/station/\<id\>/registros | No | Listado de los últimos registros de la estación. Necesita el identificador de la estación |
| /api/station/\<id\>/estadisticas | No | Listado de los últimos registros de la estación. Necesita el identificador de la estación |
| /api/estacion | No | Listado de estaciones |
| /api/estacion/\<id\> | No | Información detallada de la estación. Necesita el identificador de la estación |
| /api/estacion/\<id\>/registros | No | Listado de los últimos registros de la estación. Necesita el identificador de la estación |
| /api/estacion/\<id\>/estadisticas | No | Listado de los últimos registros de la estación. Necesita el identificador de la estación |
| /api/record/\<id\> | Si | Información detallada del registro meteorológico. Necesita el identificador del registro |
| /api/data/\<clave_lectura\> | No | Información detallada de la estación. Necesita la clave de lectura de la estación |
| /api/data/\<clave_lectura\>/registros | No | Listado de los últimos registros de la estación. Necesita la clave de lectura de la estación |
| /api/data/\<clave_lectura\>/estadisticas | No | Listado de los últimos registros de la estación. Necesita la clave de lectura de la estación |


## Detalle de las respuestas de API Rest

Las distintas peticiones que se pueden realizar al API de EMA Center devuelven
información que se detalla a continuación:

| URL | Datos de la respuesta |
|-----|-----------------------|
| /api/user | id, username, full_name, email, is_active, is_staff, is_superuser |
| /api/user/\<id\> | id, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined |
| /api/station | id_estacion, nombre, modelo, pais, ciudad, calle, latitud, longitud, altura, fecha_ultimo_registro, registros, estadisticas |
| /api/station/\<id\> | id_estacion, nombre, modelo, pais, ciudad, calle, latitud, longitud, altura, fecha_ultimo_registro, registros, estadisticas |
| /api/station/\<id\>/registros | id_registro, fecha, fecha_ingreso, software, viento_direccion_nombre, temperatura_externa, temperatura_interna, humedad_externa, humedad_interna, punto_de_rocio, luxer_intencidad, luxer_uv, viento_velocidad, viento_rafagas, viento_direccion, presion_absoluta, presion_relativa, lluvia_actual, lluvia_acumulado_hora, lluvia_acumulado_diario, lluvia_acumulado_semanal, lluvia_acumulado_mensual, lluvia_acumulado_anual, lluvia_acumulado_total |
| /api/station/\<id\>/estadisticas | hora, temperatura_externa_max, temperatura_externa_min, humedad_externa_max, lluvia_acumulado_hora_max, lluvia_acumulado_hora_min, luxer_intencidad_max, luxer_intencidad_min, viento_velocidad_max, viento_velocidad_min, presion_absoluta_max, presion_absoluta_min, luxer_uv_max, luxer_uv_min, presion_relativa_max, presion_relativa_min, humedad_externa_min, viento_rafagas_max |
| /api/estacion | id_estacion, nombre, modelo, pais, ciudad, calle, latitud, longitud, altura, fecha_ultimo_registro, registros, estadisticas |
| /api/estacion/\<id\> | id_estacion, nombre, modelo, pais, ciudad, calle, latitud, longitud, altura, fecha_ultimo_registro, registros, estadisticas |
| /api/estacion/\<id\>/registros | id_registro, fecha, fecha_ingreso, software, viento_direccion_nombre, temperatura_externa, temperatura_interna, humedad_externa, humedad_interna, punto_de_rocio, luxer_intencidad, luxer_uv, viento_velocidad, viento_rafagas, viento_direccion, presion_absoluta, presion_relativa, lluvia_actual, lluvia_acumulado_hora, lluvia_acumulado_diario, lluvia_acumulado_semanal, lluvia_acumulado_mensual, lluvia_acumulado_anual, lluvia_acumulado_total |
| /api/estacion/\<id\>/estadisticas | hora, temperatura_externa_max, temperatura_externa_min, humedad_externa_max, lluvia_acumulado_hora_max, lluvia_acumulado_hora_min, luxer_intencidad_max, luxer_intencidad_min, viento_velocidad_max, viento_velocidad_min, presion_absoluta_max, presion_absoluta_min, luxer_uv_max, luxer_uv_min, presion_relativa_max, presion_relativa_min, humedad_externa_min, viento_rafagas_max |
| /api/record/\<id\> | id_registro, fecha, fecha_ingreso, software, viento_direccion_nombre, temperatura_externa, temperatura_interna, humedad_externa, humedad_interna, punto_de_rocio, luxer_intencidad, luxer_uv, viento_velocidad, viento_rafagas, viento_direccion, presion_absoluta, presion_relativa, lluvia_actual, lluvia_acumulado_hora, lluvia_acumulado_diario, lluvia_acumulado_semanal, lluvia_acumulado_mensual, lluvia_acumulado_anual, lluvia_acumulado_total |
| /api/data/\<clave_lectura\> | id_estacion, nombre, modelo, pais, ciudad, calle, latitud, longitud, altura, fecha_ultimo_registro, registros, estadisticas |
| /api/data/\<clave_lectura\>/registros | id_registro, fecha, fecha_ingreso, software, viento_direccion_nombre, temperatura_externa, temperatura_interna, humedad_externa, humedad_interna, punto_de_rocio, luxer_intencidad, luxer_uv, viento_velocidad, viento_rafagas, viento_direccion, presion_absoluta, presion_relativa, lluvia_actual, lluvia_acumulado_hora, lluvia_acumulado_diario, lluvia_acumulado_semanal, lluvia_acumulado_mensual, lluvia_acumulado_anual, lluvia_acumulado_total |
| /api/data/\<clave_lectura\>/estadisticas | hora, temperatura_externa_max, temperatura_externa_min, humedad_externa_max, lluvia_acumulado_hora_max, lluvia_acumulado_hora_min, luxer_intencidad_max, luxer_intencidad_min, viento_velocidad_max, viento_velocidad_min, presion_absoluta_max, presion_absoluta_min, luxer_uv_max, luxer_uv_min, presion_relativa_max, presion_relativa_min, humedad_externa_min, viento_rafagas_max |

