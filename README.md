# EMA Libre Center :: Capibara

Ema Center es Software Libre y Gratuito, y el modulo principal del proyecto
EMA Libre, tiene como objetivo validar, catalogar y almacenar en una base de
datos información meteorológica recibida por cualquier software que implemente
su API REST publica, permitiendo de esta forma crear una Red Abierta de EMA
Libre (Red Abierta de Estaciones Meteorológicas Automáticas Libre).

Es importante destacar que Ema Center almacena datos que son enviados
principalmente por nuestro software oficial cliente “Ema Libre Carpincho”,
pero nada impide que una aplicación de un tercero utilizar esta API REST ya que
la misma es publica y libre.

Este software se encuentra desarrollado y mantenido por el Laboratorio de
Investigación Gugler perteneciente a la Facultad de Ciencia y Tecnología de la
Universidad Autónoma de Entre Ríos, toda esta tarea se realiza utilizando
únicamente software libre y publicado bajo la licencia GPL v3.

## Versión

Versión Actual: EMA Libre Center capibara v1.1.0

## URLs del proyecto

* [Servidor/Recolector de datos](https://emacenter.gugler.com.ar/ "EmaCenter")
* [Visor de datos via web](https://ema.gugler.com.ar/ "EmaVisor")

## Equipo de desarrollo

* Alexis Sostersich (alexis@gugler.com.ar)
* Exequiel Aramburu (exequiel@gugler.com.ar)
* Federico Bonnet (fede@gugler.com.ar)
* Marcos Ríos (marcos@gugler.com.ar)
* Martín Sbarbaro (martin@gugler.com.ar)


## Obten el código

Para empezar a contribuir con el código de EMA Center, necesitas hacer una copia
del repositorio principal de desarrollo:
```
git clone http://git.gugler.com.ar/emalibre/emacenter.git

```

## Despliega en desarrollo

Una vez que tengas el repositorio en tu equipo, tienes que ejecutar los scripts 
que deja todo el entorno preparado.

Ingresa al directorio de tu repositorio de EMA Center, creamos el entorno
virtual e instalamos las dependencias:
```
cd emacenter
python -m venv venv
source venv/bin/activate
pip install -U pip
pip install -r requirements.dev.txt

```
Posteriormente podemos ejecutar emacenter:
```
./manage runserver

```

## Probando EMA Center

Con esto tienes los comandos de cualquier aplicación Django a tu disposición. 
Por ejemplo, puedes revisar que todo este correcto:
```
./manage check
```
Para ejecutar todas las pruebas unitarias de la aplicación:
```
./manage test
```
Para ejecutar el servidor web de prueba y acceder mediante tu navegador a `http://localhost:8000`:
```
./manage runserver

```

## Implementando EMA Center

Generamos la imagen del container:
```
docker build -t emacenter:latest .
```
o
```
podman build -t emacenter:latest .
```

Necesitamos al menos configurar la base de datos de EMACenter en el archivo
`environment.conf`:
```
DATABASE_URL=postgres://postgres:clave@localhost:5432/emacenter_db
```
Ahora podemos ejecutar nuestra imagen:
```
podman run -p 8000:8000 --env-file environment.conf localhost/emacenter:latest
```

[![N|Solid](https://ema.gugler.com.ar/imgs/LogoLGPLv3-mobile.png)](https://www.gnu.org/licenses/gpl-3.0.en.html)
[![N|Solid](https://ema.gugler.com.ar/imgs/logo_gugler_lab-mobile.png)](https://www.gugler.com.ar)
[![N|Solid](https://ema.gugler.com.ar/imgs/logo_uader_mini-mobile.png)](http://fcyt.uader.edu.ar/web/)
