# Proyecto de Investigación EMA Libre

EMA Libre Center nace en el 2018 de la formulación de un proyecto de 
Investigación, y es parte de un conjunto de tres sistema EMA Libre Carpincho, 
EMA Libre Center y EMA Liber Visor.

Desarrollado por el Laboratorio de Investigación Gugler perteneciente a la 
Facultad de Ciencia y Tecnología de la Universidad Autónoma de Entre Ríos. 
A continuación se detalla la lista de colaboradores y desarrolladores:

* Exequiel Aramburu <exequiel@gugler.com.ar>.
* Mario Martin Sbarbaro <martin@gugler.com.ar>.
* José Luis Mengarelli <jlmenga@gugler.com.ar>.
* Federico Bonnet <federico@gugler.com.ar>.
* Alexis Sostersich <alexis@gugler.com.ar>.
* Marcos Elias Rios Nuñez <marcos@gugler.com.ar>.

Se agradece a la Facultad de Ciencia y Tecnología de la Universidad Autónoma de 
Entre Ríos por permitir la conformación de este equipo interdisciplinario de
profesionales y alumnos colaboradores. Para llevar acabo una investigación de 
esta magnitud, permitiendo mostrar las cualidades y bondades del Software Libre.

Finalmente, no se solicitó declaración de renuncia de copyright a la facultad, 
ya que por Medio de la Resolución CS Nº 063/18, el Consejo Directivo de la 
Facultad de Ciencia y Tecnología de la Universidad Autónoma de Entre Ríos, 
aprueba el proyecto Investigación, el cual especifica que el software utilizará 
una licencia libre.
